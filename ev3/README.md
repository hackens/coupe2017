
To compile, you must use a docker image. First download docker and launch its
service with `systemctl start docker`. Test the installation with
`docker run hello-world` (you may need to execute docker as root).
Then download the ev3dev image with the command
`docker pull ev3dev/debian-jessie-cross`, and give it a surname with
`docket tag ev3dev/debian-jessie-cross ev3cc`.

Now run `docker run --rm -it -v $PWD:/src -w /src ev3cc` to get a shell where
you can build with make. The produced binary is accessible even outside the
shell.

