#ifndef MOTION_H
#define MOTION_H

#include "distance.h"
#include "angle.h"

/// Describe a mouvement
class Motion {
  public:
    Motion();
    Motion(Distance d, Angle dt, bool col = true);

    Distance d() const noexcept;
    Angle dt() const noexcept;
    /// \brief execute the motion
    ///
    /// send command to the Serial object Robot::sStepper
    /// and set Robot::sDirLeft and Robot::sDirRight
    void execute();
  private:
    Distance mD=0;
    Angle mDt=0;
    bool mCol;
};

#endif
