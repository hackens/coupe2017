#include "triangulation.hpp"

#include <math.h>

//http://www.telecom.ulg.ac.be/triangulation/

bool triangulate(float *x, float *y, float *alpha,
                    float alpha1, float alpha2, float alpha3,
                    float x1, float y1,
                    float x2, float y2,
                    float x3, float y3) {
    float cot_12 = 1/tan( alpha2 - alpha1 ) ;
    float cot_23 = 1/tan( alpha3 - alpha2 ) ;
    float cot_31 = ( 1.0 - cot_12 * cot_23 ) / ( cot_12 + cot_23 ) ;
    if(cot_12 > 1000000 || cot_23 > 1000000 || cot_31 > 1000000) {
        return false; //This will fail
    }

    float x1_ = x1 - x2 , y1_ = y1 - y2 , x3_ = x3 - x2 , y3_ = y3 - y2;
    float c12x = x1_ + cot_12 * y1_ ;
    float c12y = y1_ - cot_12 * x1_ ;

    float c23x = x3_ - cot_23 * y3_ ;
    float c23y = y3_ + cot_23 * x3_ ;

    float c31x = (x3_ + x1_) + cot_31 * (y3_ - y1_) ;
    float c31y = (y3_ + y1_) - cot_31 * (x3_ - x1_) ;
    float k31 = (x3_ * x1_) + (y3_ * y1_) + cot_31 * ( (y3_ * x1_) - (x3_ * y1_) ) ;

    float D = (c12x - c23x) * (c23y - c31y) - (c23x - c31x) * (c12y - c23y) ;
    float invD = 1.0 / D ;
    float K = k31 * invD ;

    *x = K * (c12y - c23y) + x2 ;
    *y = K * (c23x - c12x) + y2 ;

    return true;
}


bool find_position(float* x, float* y, float* alpha,
                   float approx_x, float approx_y,
                   float alpha1, float alpha2, float alpha3,
                   float x1, float y1,
                   float x2, float y2,
                   float x3, float y3) {
    float px1,py1,palpha1,px2,py2,palpha2,px3,py3,palpha3;
    bool b1,b2,b3;
    b1 = triangulate(&px1,&py1,&palpha1,alpha1,alpha2,alpha3,x1,y1,x2,y2,x3,y3);
    b2 = triangulate(&px2,&py2,&palpha2,alpha2,alpha3,alpha1,x2,y2,x3,y3,x1,y1);
    b3 = triangulate(&px3,&py3,&palpha3,alpha3,alpha1,alpha2,x3,y3,x1,y1,x2,y2);
    float d1 = (px1-approx_x)*(px1-approx_x) + (py1-approx_y)*(py1-approx_y);
    float d2 = (px2-approx_x)*(px2-approx_x) + (py2-approx_y)*(py2-approx_y);
    float d3 = (px3-approx_x)*(px3-approx_x) + (py3-approx_y)*(py3-approx_y);
    if(!b1 || !b2 || !b3) {
        return false;
    }
    if(d1 < d2 && d1 < d3) {
        *x = px1;
        *y = py1;
        *alpha = palpha1;
    } else if(d2 < d1 && d2 < d3) {
        *x = px2;
        *y = py2;
        *alpha = palpha2;
    } else{
        *x = px3;
        *y = py3;
        *alpha = palpha3;
    }
    return true;
}
