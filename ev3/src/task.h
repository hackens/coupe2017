#pragma once

#include <string>
#include <vector>
#include "vector.h"
#include "graph.h"

class PrimTask;

namespace Behavior{

  class Task{
    public:
      explicit Task(std::string name);
      virtual ~Task(){}

      /// do one step of the task, the boolean at true means the task is not finished
      virtual bool step() =0;

      /// return true when task has failed
      virtual bool failed() = 0;

      void execute(){
        while(step());
      }

      bool done() {return mDone;}
    protected:
      std::string mName;
      bool mDone;
  };

  class DirectGoTo : public Task{
      Vector2 mDest;
      Distance mEpsilon;
      Angle mAlpha;
      bool mCol;
    public:
      /// go directly to dest with precision epsilon in Distance and Alpha in angle.
      DirectGoTo(std::string name,Vector2 dest,Distance epsilon = 0.1_cm,Angle alpha = 0.03,bool col = true);
      bool step();
      bool failed(){return false;}

  };

  //
  class GotoGraph : public Task{
      int currentDest;
      int finalDest;
      Graph& mGraph;
    public :
      GotoGraph(const std::string& name, Graph& g, int startVert, int destVert);
      bool step();
      bool failed(){return false;}
  };

  /// Goto orientation and empty the basket
  class EmptyBasket : public Task{
      Angle mOrientation;
    public:
      EmptyBasket(const std::string& name,
                  Angle orientation);
      bool step();
  };

};
