#include "task.h"
#include "taskPrim.h"
#include <cmath>
#include <iostream>
#include <cassert>
#include "logging.h"
#include "motion.h"
#include "control.hpp"
#include "position.h"

using namespace std;
using namespace Logging;
using namespace Behavior;

Task::Task(string name) : mName(name), mDone(false)
{
}

DirectGoTo::DirectGoTo(string name,Vector2 dest,Distance epsilon,Angle alpha,bool col)
  : Task(move(name)),mDest(dest),mEpsilon(epsilon),mAlpha(alpha),mCol(col){
      info << "Init goto with " << dest << "\n";

}


bool DirectGoTo::step(){
  Vector2 movement = mDest - pos.mPosition;
  Angle aMov = movement.orientation() - pos.mOrientation;
  info << "DGT going to" << mDest << "from" << pos.mPosition << "\n";
  debug << "DGT movement : " << movement << "and rotation " << aMov << "\n";

  if(movement.Norm() < mEpsilon){
    mDone = true;
    return false;
  }
  if (std::abs(aMov) > mAlpha){
    Motion mov(0, aMov, mCol);
    mov.execute();
    return true;
  }
  if(movement.Norm() > mEpsilon){
    Motion mov(movement.Norm(), 0, mCol);
    mov.execute();
  }
  mDone = true;
  return false;
}


/*  ____       _         ____                 _
   / ___| ___ | |_ ___  / ___|_ __ __ _ _ __ | |__
  | |  _ / _ \| __/ _ \| |  _| '__/ _` | '_ \| '_ \
  | |_| | (_) | || (_) | |_| | | | (_| | |_) | | | |
   \____|\___/ \__\___/ \____|_|  \__,_| .__/|_| |_|
                                       |_|
*/

GotoGraph::GotoGraph(const string& name, Graph& g, int startVert, int destVert)
  :Task(name), currentDest(startVert), finalDest(destVert), mGraph(g) {
}


bool GotoGraph::step(){
  DirectGoTo dgt(mName + " DGT",mGraph[currentDest].pos());
  if(dgt.step()) return true;
  if(currentDest == finalDest) {
    mDone  = true;
    return false;
  }
  // next vertex;
  vector<Distance> smallest(mGraph.size(),1.0/0.0);
  vector<int> parent(mGraph.size(),-1);
  set<int> toUpdate = {finalDest};
  set<int> toUpdateNext;

  while(!toUpdate.empty()){
    for(auto i : toUpdate){
      if(!mGraph[i].active()) continue;
      for(auto neigh : mGraph[i].neigh()){
        if(smallest[neigh] > smallest[i] + mGraph[i].dist(mGraph[neigh])){
          parent[neigh] = i;
          toUpdateNext.insert(neigh);
        }
      }
    }
    toUpdate = move(toUpdateNext);
  }

  currentDest = parent[currentDest];
  assert(currentDest != -1);
  return step();
}


/* _____                 _         ____            _        _
  | ____|_ __ ___  _ __ | |_ _   _| __ )  __ _ ___| | _____| |_
  |  _| | '_ ` _ \| '_ \| __| | | |  _ \ / _` / __| |/ / _ \ __|
  | |___| | | | | | |_) | |_| |_| | |_) | (_| \__ \   <  __/ |_
  |_____|_| |_| |_| .__/ \__|\__, |____/ \__,_|___/_|\_\___|\__|
                  |_|        |___/
*/

EmptyBasket::EmptyBasket(const std::string& name,
            Angle orientation)
  : Task(name), mOrientation(orientation){

}

bool EmptyBasket::step(){
    Control::ctrl->empty();
    Control::ctrl->lower();
    return false;
}
