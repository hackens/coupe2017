#pragma once

#include <string>

namespace Behavior
{
  class TaskPrim{

  public:
    explicit TaskPrim(std::string name);
    virtual void execute() =0;

  private:
    std::string mName;
  };

};
