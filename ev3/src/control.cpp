
#include "control.hpp"
#include <cmath>
using namespace ev3dev;

const int dist_col = 60;
	
Control* Control::ctrl;

Control::~Control() {
    reset();
}

void Control::fast() {
    _speed = true;
}

void Control::slow() {
    _speed = false;
}

void Control::forward(int millimeters) {
    int speed = 150;
    int tacko = (millimeters * 186) / 100;
    int ramp = 3000;
    if(!_speed) {
        speed /= 2;
    }

    _left.set_speed_sp(speed);
    _right.set_speed_sp(speed);
    _left.set_ramp_up_sp(ramp);
    _right.set_ramp_up_sp(ramp);

    _left.set_position_sp(tacko).run_to_rel_pos();
    _right.set_position_sp(tacko).run_to_rel_pos();
}

void Control::turn(int angle) {
    const int speed = 100;
    int tacko = (angle * 130) / 10;
    int ramp = 3000;
    if(!side()) tacko = -tacko;

    _left.set_speed_sp(speed);
    _right.set_speed_sp(speed);
    _left.set_ramp_up_sp(ramp);
    _right.set_ramp_up_sp(ramp);

    _left.set_position_sp(-tacko).run_to_rel_pos();
    _right.set_position_sp(tacko).run_to_rel_pos();
}
        
bool Control::moving() {
    if(_left.state().count("running")  || _left.state().count("ramping"))  return true;
    if(_right.state().count("running") || _right.state().count("ramping")) return true;
    return false;
}
        
void Control::stop() {
    _left.reset();
    _right.reset();
}

void Control::reset() {
    try { stop(); } catch(...) { }
    try { stop_raising(); } catch(...) { }
    try { _emptying.reset(); } catch(...) { }
}

void Control::start_raising() {
    const int speed = -500;
    _rising.set_speed_sp(speed);
    _rising.run_forever();
}

void Control::stop_raising() {
    _rising.reset();
}

void Control::empty() {
    const int speed = 1000;
    const int time = 8500;
    _emptying.set_speed_sp(speed);
    _emptying.set_time_sp(time).run_timed();
    while(_emptying.state().count("running") || _emptying.state().count("ramping"));
}

void Control::lower() {
    const int speed = -1000;
    const int time = 8500;
    _emptying.set_speed_sp(speed);
    _emptying.set_time_sp(time).run_timed();
    while(_emptying.state().count("running") || _emptying.state().count("ramping"));
}

bool Control::has_started() {
    return _start.color() == 4 || _start.color() == 5;
}

int Control::side() {
    return _start.color() == 4;
}

bool Control::must_stop() {
    bool b;
again:
    try {
        b = _urgent.is_pressed(false);
    } catch(...) {
        goto again;
    }
    return b;
}

int Control::distance() {
    return _dist.proximity(false);
}
        
void Control::turnRadian(float radians) {
    turn((int)(radians / M_PI * 50.0));
    while(moving());
}

void Control::move(int millimeters) {
    forward(millimeters);
    while(moving()) {
        if(distance() <= dist_col) {
            _right.stop();
            _left.stop();
            while(distance() <= dist_col);
            _right.run_to_rel_pos();
            _left.run_to_rel_pos();
        }
    }
}

void Control::enable_col() {
    _col = true;
}

void Control::disable_col() {
    _col = false;
}

