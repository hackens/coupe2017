
#ifndef DEF_CONTROL
#define DEF_CONTROL

#include "ev3dev.h"

class Control {
    ev3dev::large_motor     _left;
    ev3dev::large_motor     _right;
    ev3dev::large_motor     _rising;
    ev3dev::medium_motor    _emptying;
                            
    ev3dev::touch_sensor    _urgent;
    ev3dev::color_sensor    _start;
    ev3dev::infrared_sensor _dist;
    bool                    _speed;
    bool                    _col;

    public:
	static Control* ctrl;
        Control()
            : _left(ev3dev::OUTPUT_B), _right(ev3dev::OUTPUT_C),
              _rising(ev3dev::OUTPUT_A), _emptying(ev3dev::OUTPUT_D),
              _urgent(ev3dev::INPUT_2), _start(ev3dev::INPUT_3),
              _dist(ev3dev::INPUT_4), _speed(true), _col(true)
        {
            _urgent.set_mode(_urgent.mode_touch);
        }
        ~Control();

        void forward(int millimeters);
        void turn(int angle); /* angle is in 1/100 full turn */
        bool moving();
        void stop();
        void reset();
        void start_raising();
        void stop_raising();
        /* The two following are blocking */
        void empty(); /* Raise the container */
        void lower(); /* Lower the container */
        void fast();
        void slow();

        /* Blocking */
        void turnRadian(float radians);
        void move(int millimeters);
        void enable_col();
        void disable_col();

        bool has_started();
        /* Yellow side ? */
        int side();
        bool must_stop();
        /* Proximity of obstacles in % */
        int distance();
};

#endif//DEF_CONTROL

