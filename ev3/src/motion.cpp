#include "motion.h"
#include "logging.h"
#include "position.h"
#include "control.hpp"

using namespace Logging;


Motion::Motion() {
}

Motion::Motion(Distance d, Angle dt, bool col) : mD(d), mDt(dt), mCol(col) {
}

Distance Motion::d() const noexcept {
  return mD;
}

Angle Motion::dt() const noexcept {
  return mDt;
}
void Motion::execute(){
  if(mDt == 0){
    debug << "Motion : moving straight of " << mD << "\n";
    if(mCol) Control::ctrl->enable_col();
    else     Control::ctrl->disable_col();
    Control::ctrl->move(mD.millimeters());
    pos.mPosition += Vector2(mD,pos.mOrientation);

  }
  if(mD.zero()){
    debug << "Motion : turning of " << mDt << "\n";
    Control::ctrl->turnRadian(mDt);
    pos.mOrientation += mDt;
  }
}
