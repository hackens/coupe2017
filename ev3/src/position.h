#ifndef POSITION_H
#define POSITION_H

#include"vector.h"

class Position{
  public:
    Vector2 mPosition;
    Angle mOrientation;
    // update current position according to the laser turret results.
    void updateTurret();
    Position(Vector2 startPos, Angle startOri);
};

extern Position pos;

#endif
