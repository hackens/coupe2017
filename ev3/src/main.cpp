
#include <iostream>
#include <unistd.h>
#include <string>
#include <cmath>
#include "control.hpp"
#include "motion.h"
#include "vector.h"
#include "task.h"
#include "position.h"
using namespace std;

#define exe(e) while((e).step());
void execute_p1() {
    Control* ctrl = Control::ctrl;
    Behavior::DirectGoTo out("out", Vector2(Distance(0.23), Distance(1.0)));
    Behavior::DirectGoTo step1("step1", Vector2(Distance(0.6), Distance(1.0)));
    Behavior::DirectGoTo balls("balls", Vector2(Distance(0.6), Distance(0.6)));
    Motion back(-0.6,0);
    Motion reverse(0,M_PI);
    Motion back2(-0.5,0);

    exe(out);
    exe(step1);
    ctrl->slow();
    ctrl->start_raising();
    exe(balls);
    back.execute();
    reverse.execute();
    back2.execute();
    ctrl->empty();
}

int main() {
    Control::ctrl = new Control;
    Control* ctrl = Control::ctrl;
    string cmd;
    int a1, a2;
    pos.mPosition = Vector2(Distance(0.23), Distance(0.23));
    pos.mOrientation = M_PI / 2;

again:
    try {
        while(!ctrl->has_started()) {
            usleep(100000);
        }
    } catch(...) {
        goto again;
    }

    execute_p1();
    /*
    while(1) {
        cout << "> ";
        cin >> cmd;
        if(cmd == "forward" || cmd == "f") {
            cin >> a1;
            Motion mov((double)a1 / 1000.0, 0);
            mov.execute();
        } else if(cmd == "turn" || cmd == "t") {
            cin >> a1;
            Motion mov(0, (double)a1 / 50.0 * M_PI);
            mov.execute();
        } else if(cmd == "goto" || cmd == "g") {
            cin >> a1 >> a2;
            Vector2 vec(Distance((double)(a1) / 1000.0), Distance((double)(a2) / 1000.0));
            Behavior::DirectGoTo task("goto task", vec);
            while(task.step());
        } else if(cmd == "distance" || cmd == "d") {
            cout << "Distance " << ctrl->distance() << endl;
        } else if(cmd == "start") {
            ctrl->start_raising();
        } else if(cmd == "stop") {
            ctrl->stop_raising();
        } else if(cmd == "empty") {
            ctrl->empty();
        } else if(cmd == "lower") {
            ctrl->lower();
        } else if(cmd == "fast") {
            ctrl->fast();
        } else if(cmd == "slow") {
            ctrl->slow();
        } else if(cmd == "stop") {
            break;
        }
    }
    */

    ctrl->reset();
    return 0;
}

