#ifndef TRIANGULATION_HPP
#define TRIANGULATION_HPP


// Returns the absolute position given the absolute position
// of 3 known beacons, and their angles relative to the robot
bool triangulate(float* x, float* y, float* alpha,
                 float alpha1, float alpha2, float alpha3,
                 float x1, float y1,
                 float x2, float y2,
                 float x3, float y3);


bool find_position(float* x, float* y, float* alpha,
                   float approx_x, float approx_y,
                   float alpha1, float alpha2, float alpha3,
                   float x1, float y1,
                   float x2, float y2,
                   float x3, float y3);

#endif // TRIANGULATION_HPP
