
#include <iostream>
#include <unistd.h>
#include <string>
#include <unistd.h>
#include <time.h>
#include "control.hpp"
using namespace std;

int main() {
    Control::ctrl = new Control;
    Control* ctrl = Control::ctrl;

again:
    try {
        while(!ctrl->has_started()) {
            usleep(100000);
        }
    } catch(...) {
        goto again;
    }

    long long t = time(NULL);
    while(!ctrl->must_stop() && time(NULL) - t <= 90) {
        usleep(100000);
    }
    // ctrl->reset();
    system("poweroff");
    return 0;
}

