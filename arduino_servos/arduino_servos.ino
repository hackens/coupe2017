#include <Servo.h>

Servo s3, s5, s6, s9, s10, s11;
// s3
// s5 belt
// s6 lock front
// s9 lock back
// s10 grabber
// s11 lift

unsigned long long until;


/**
 * Functions used to get sensors (but in a more readable way
 */

bool cylinderInFront() {
  return !get(1);
}

bool cylinderInBack() {
  return !get(2);
}

bool isElevatorUp() {
  return get(3);
}

bool hasMatchStarted() {
  return get(4);
}

void abort() {
  s5.detach();
  s6.detach();
  s9.detach();
  s10.detach();
  s11.write(90);
  delay(2000);
  launchrocket();
  while(1){
    Serial.println("Abort");
    delay(1000);
  }
}


String init_timer(unsigned long long mm) {
  until = (unsigned long long)mm * 1000LL + (unsigned long long)millis();
  return "Done" + String((long)until);
}



void delay2(unsigned long long time) {
  unsigned long long t = millis();
  while(millis() - t < time && until > millis()) ;
  if(until < millis()) {
      abort();
  }
}




/**
 * Functions to do things physically
 */

void lift() {
  s11.write(60);
  unsigned long long t = millis();
  while (!get(3) && until > (unsigned long long)(millis()) && millis()-t < 10000) {
  }
  if(until < (unsigned long long)(millis())) {
      abort();
  }
  s11.write(90);
}

void lower() {
  s11.write(105);
  delay2(4000);
  s11.write(90);
}

void up() {
  s11.write(60);
  unsigned long long debut = millis();
  unsigned long long actuel = millis();
  while(!get(3) && actuel-debut < 500 && until > (unsigned long long)(millis())) {
    actuel = millis();
  }
  if(until < (unsigned long long)(millis())) {
      abort();
  }
  s11.write(90);
}

void down() {
  s11.write(105);
  delay2(500);
  s11.write(90);
}

void unlock() {
  Servo* front = &s6;
  Servo* back = &s9;
  back->write(200);
  front->write(30);
  delay2(200);
  back->write(195);
  front->write(35);
  front->detach();
  back->detach();
}

void lock() {
  s6.attach(6);
  s9.attach(9);
  Servo* front = &s6;
  Servo* back = &s9;
  back->write(40);
  front->write(190);
  delay2(200);
  back->write(80);
  front->write(110);
  delay2(200);
  back->write(70);
  front->write(120);
}

void grab() {
  Servo* g = &s10;
  s10.attach(10);
  g->write(0);
  delay2(100);
  g->write(120);
  delay2(1000);
}

void release_() {
  Servo* g = &s10;
  s10.attach(10);
  g->write(0);
  delay2(1000);
  s10.detach();
}

void grabStrait(){
  s10.attach(10);
  s10.write(0);
  delay2(500);
  s10.write(100);
  delay2(500);
}

void lockrocket() {
  s3.attach(3);
  s3.write(180);
  delay(1000);
  s3.detach();
}
void launchrocket() {
  s3.attach(3);
  s3.write(0);
  delay(500);
  s3.detach();
}


void lockfront() {
  Servo* g = &s10;
  s10.attach(10);
  g->write(0);
  delay2(500);
  g->write(105);
  delay2(500);
}

String grabsequence() {
  //release_();
  if (!cylinderInFront())
    return "Error: Nothing to grab";
  if (cylinderInBack())
    return "Error : Something already in back";
    
  grab();
  beltPull();
  delay2(3000);
  beltStop();
  s10.write(105);  //Release the pressure a bit
  delay2(500);
  beltPull();
  delay2(1000);
  s10.write(95);  //Set the belt straight
  unsigned long long t = millis();
  while (!(cylinderInBack() || millis()-t>12000) && until > millis()) ;
  if(until < millis()) {
    abort();
  }
  delay2(500);
  beltStop();
  return "Done";
}

void releasesequence() {
  //if (get(2))
  //  return;
  grabStrait();
  
  beltPush();
  delay2(1500);
  delay2(10000);
  beltStop();
  release_();
}

/**
 * There should be nothing on the elevator yet
 */
String graballsequence() {
  int nbCylinders = 0;
  bool stop = false;
  if (cylinderInBack())
    return "Error : Something already in back";
  unlock();
  lift();
  lower();
  if (!cylinderInFront())
    return "Error: Nothing to grab";
  nbCylinders++;
  grab();
pull:
  beltPull();
  delay2(4000);
  grabStrait();
  while (!cylinderInBack() && until > millis()) ;
  if(until < millis()) {
      abort();
  }
  delay2(500);
  beltStop();
  release_();
  if(!cylinderInFront()) stop = true;
  else grab();
  beltPull();
  unlock();
  while(cylinderInBack() && until > millis()) {
    lift();
  }
  if(until < millis()) {
      abort();
  }
  lock();
  beltStop();
  lower();
  if(!stop) goto pull;
  //sprintf("Done %d",i);
  return "Done";
}


String releaseallsequence() {
  int nbCylinders = 0;
  release_();
  grabStrait();
  while(until > millis()) {
    beltStop();
    unlock();
    lock();
    if(!cylinderInBack()) {
      return "Nothing to release";
    }
    beltPush();
    while(!cylinderInFront() && until > millis());
    if(until < millis()) {
      abort();
    }
    delay2(1000);
  }
  if(until < millis()) {
    abort();
  }
  delay2(2000);
}


void beltPull() {
  s5.attach(5);
  s5.write(90);
  delay2(300);
  s5.write(0);
}

void beltStop() {
  s5.write(90);
  delay2(300);
  s5.detach();
}

void beltPush() {
  s5.attach(5);
  s5.write(90);
  delay2(500);
  s5.write(360);
}

int get(int i) {
  switch (i) {
    case 1:
      return digitalRead(2);
    case 2:
      return digitalRead(4);
    case 3:
      return digitalRead(7);
    case 4:
      return digitalRead(8);
    case 5:
      return digitalRead(A0);
    case 6:
      return digitalRead(A1);
  }
}

String getstr(int i) {
  int k = get(i);
  if (k)
    return "1";
  else
    return "0";
}

void setup() {
  until = (unsigned long long)(-1);
  for (int i=1; i<13; i++) {
    pinMode(i,INPUT);
  }
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A3,INPUT);
  pinMode(A4,INPUT);
  pinMode(A5,INPUT);
  
  pinMode(13,OUTPUT);
  digitalWrite(13,0);
  
  Serial.begin(9600);

  s3.detach();
  s5.detach();
  s6.detach();
  s9.detach();
  s10.detach();
  s11.attach(11);
  s11.write(90);
}

String serialBuffer;


String getSerialLine() {
  while (Serial.available()>0 && until > millis()) {
    char c = Serial.read();
    Serial.print(c);
    if (c=='\n' || c=='\r') {
      String copy=serialBuffer;
      serialBuffer="";
      return copy;
    }
    serialBuffer.concat(c);
  }
  if(until < millis()) {
      abort();
  }
  return "";
}

void handle(Servo s, String l) {
    float len = l.substring(3).toFloat();
    Serial.print("Set servo to ");
    Serial.println(len);
    s.write(len);
}

void loop() {
  String l = getSerialLine();
  if (l.startsWith("ON ")) {
    //float len = l.substring(5).toFloat();
    Serial.println("Led on");
    digitalWrite(13,1);
  }
  else if (l.startsWith("OFF ")) {
    //float len = l.substring(5).toFloat();
    Serial.println("Led off");
    digitalWrite(13,0);
  }

  else if (l.startsWith("A1 ")) {
    s3.attach(3);
    Serial.println("Attaching servo 1 (pin 3)");
  }
  else if (l.startsWith("A2 ")) {
    s5.attach(5);
    Serial.println("Attaching servo 2 (pin 5)");
  }
  else if (l.startsWith("A3 ")) {
    s6.attach(6);
    Serial.println("Attaching servo 3 (pin 6)");
  }
  else if (l.startsWith("A4 ")) {
    s9.attach(9);
    Serial.println("Attaching servo 4 (pin 9)");
  }
  else if (l.startsWith("A5 ")) {
    s10.attach(10);
    Serial.println("Attaching servo 5 (pin 10)");
  }
  else if (l.startsWith("A6 ")) {
    Serial.println("Attaching servo 6 (pin 11) but is always attached");
  }
  
  else if (l.startsWith("D1 ")) {
    s3.detach();
    Serial.println("Detaching servo 1 (pin 3)");
  }
  else if (l.startsWith("D2 ")) {
    s5.detach();
    Serial.println("Detaching servo 2 (pin 5)");
  }
  else if (l.startsWith("D3 ")) {
    s6.detach();
    Serial.println("Detaching servo 3 (pin 6)");
  }
  else if (l.startsWith("D4 ")) {
    s9.detach();
    Serial.println("Detaching servo 4 (pin 9)");
  }
  else if (l.startsWith("D5 ")) {
    s10.detach();
    Serial.println("Detaching servo 5 (pin 10)");
  }
  else if (l.startsWith("D6 ")) {
    //s11.detach();
    Serial.println("Not detaching servo 6 (pin 11)");
  }
  
  else if (l.startsWith("S1 ")) {
    handle(s3,l);
  }
  else if (l.startsWith("S2 ")) {
    handle(s5,l);
  }
  else if (l.startsWith("S3 ")) {
    handle(s6,l);
  }
  else if (l.startsWith("S4 ")) {
    handle(s9,l);
  }
  else if (l.startsWith("S5 ")) {
    handle(s10,l);
  }
  else if (l.startsWith("S6 ")) {
    handle(s11,l);
  }
  else if (l.startsWith("GRAB ")) {
    Serial.print("Grabbing... ");
    grab();
    Serial.println("done");
  }
  else if (l.startsWith("RELEASE ")) {
    Serial.print("Releasing... ");
    release_();
    Serial.println("done");
  }
  else if (l.startsWith("LOCK ")) {
    Serial.print("Locking... ");
    lock();
    Serial.println("done");
  }
  else if (l.startsWith("UNLOCK ")) {
    Serial.print("Unlocking... ");
    unlock();
    Serial.println("done");
  }
  else if (l.startsWith("LIFT")) {
    Serial.print("Lifting... ");
    lift();
    Serial.println("done");
  }
  else if (l.startsWith("LOWER")) {
    Serial.print("Lowering... ");
    lower();
    Serial.println("done");
  }
  else if (l.startsWith("UP")) {
    Serial.print("Lifting... ");
    up();
    Serial.println("done");
  }
  else if (l.startsWith("DOWN")) {
    Serial.print("Lowering... ");
    down();
    Serial.println("done");
  }
  else if (l.startsWith("GRABSEQUENCE ")) {
    Serial.print("Running grab sequence... ");
    Serial.println(grabsequence());
  }
  else if (l.startsWith("BELTPULL")) {
    Serial.print("Pulling on belt... ");
    beltPull();
    Serial.println("done");
  }
  else if (l.startsWith("BELTPUSH")) {
    Serial.print("Pushing on belt... ");
    beltPush();
    Serial.println("done");
  }
  else if (l.startsWith("BELTSTOP")) {
    Serial.print("Stopping belt... ");
    beltStop();
    Serial.println("done");
  }
  else if (l.startsWith("BELTSTRAIGHT")) {
    Serial.print("Straightening belt... ");
    grabStrait();
    Serial.println("done");
  }
  else if (l.startsWith("GRABALLSEQUENCE ")) {
    Serial.print("Running grab all sequence... ");
    Serial.println(graballsequence());
  }
  else if (l.startsWith("RELEASESEQUENCE ")) {
    Serial.print("Running release sequence... ");
    releasesequence();
    Serial.println("done");
  }
  else if (l.startsWith("RELEASEALLSEQUENCE ")) {
    Serial.print("Running release all sequence... ");
    Serial.println(releaseallsequence());
  }
  else if (l.startsWith("LOCK_ROCKET")) {
    Serial.print("Locking rocket ... ");
    lockrocket();
    Serial.println("done");
  }
  else if (l.startsWith("LAUNCH_ROCKET")) {
    Serial.print("Launching rocket ... ");
    launchrocket();
    Serial.println("done");
  }
  else if (l.startsWith("G1 ")) {
    Serial.println(getstr(1));
  }
  else if (l.startsWith("G2 ")) {
    Serial.println(getstr(2));
  }
  else if (l.startsWith("G3 ")) {
    Serial.println(getstr(3));
  }
  else if (l.startsWith("G4 ")) {
    Serial.println(getstr(4));
  }
  else if (l.startsWith("G5 ")) {
    Serial.println(getstr(5));
  }
  else if (l.startsWith("G6 ")) {
    Serial.println(getstr(6));
  }
  else if (l.startsWith("RESET")) {
    until = (unsigned long) -1;
    Serial.println("Abort signal reseted");
  }
  else if (l.startsWith("TIMER ")) {
    unsigned int len = l.substring(6).toInt();
    String r = init_timer(len);
    Serial.println(r);
  }
  else if (l.length()>0) {
    Serial.print("Unrecognized command ");
    Serial.println(l);
  }
}

