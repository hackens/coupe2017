`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:34:19 03/08/2017 
// Design Name: 
// Module Name:    shift_transmitter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module shift_transmitter(
    input CLK,
    input [31:0] data,
    input tx_start,
    output reg tx_busy=0,
	 output reg dataOut=0,
	 output reg dataAdv=0,
	 output reg dataReady=0
    );

reg [14:0] ctrBaud = 0;
reg [4:0] ctr = 0;
reg [17:0] ctrIdle = 18'd20000;


always @(posedge CLK) begin
end

reg[1:0] latchctr=0;
reg prevbaud=0;
always @(posedge CLK) begin
	ctrBaud <= ctrBaud+1;
	if (ctrIdle<18'd20000) begin
		ctrIdle = ctrIdle+1;
	end
	else begin
		if (tx_start & !tx_busy) begin
			tx_busy = 1'b1;
			dataReady = 1'b0;
		end
	end

	if (ctrBaud[13] & !prevbaud) begin
		if (tx_busy) begin
			latchctr=latchctr+1;
			if (latchctr==0) begin
				dataOut = data[31-ctr];
				ctr = ctr+1;
			end
			else if (latchctr==1) begin
				dataAdv = 1'b1;
			end
			else if (latchctr==2) begin
				dataAdv = 1'b0;
			end
			else if (latchctr==3) begin
				if (ctr==0) begin
					dataReady = 1'b1;
					tx_busy = 1'b0;
					ctrIdle = 18'd0;
					dataAdv = 1'b0;
				end
			end
		end
	end
	prevbaud=ctrBaud[13];
end

endmodule
