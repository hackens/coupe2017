#include <SoftwareSerial.h>

const int dataIn = 10;
const int dataAdv = 11;
const int dataReady = 12;

unsigned long get() {
  unsigned long r=0;
  // Wait for transmission start
  while (!digitalRead(dataReady));
    
  for (unsigned int i=0; i<32; i++) {
    // Wait for dataAdv to be high
    while (!digitalRead(dataAdv));
    
    unsigned long b=digitalRead(dataIn);
    r |= b<<(31-i);
    
    // Wait for dataAdv to be low
    while (digitalRead(dataAdv));
  }
  
  return r;
}

void setup() {
  for (int i=0; i<13; i++) {
    pinMode(i,INPUT);
  }
  
  Serial.begin(115200);
  while (!Serial);
  
  Serial.println("AVR starting retransmission");
}

void loop() {
  Serial.println(get());
}

