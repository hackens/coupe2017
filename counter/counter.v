`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:52:55 09/10/2016 
// Design Name: 
// Module Name:    stepperRamp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module counter(
    input CLK,
	 input Arduino_2,		// ctr in
	 input Arduino_3,		// ctr reset
	 output Arduino_10, 		// data
	 output Arduino_11,		// data_adv
	 output Arduino_12,		// data_ready
	 output reg Arduino_13=1'b1, 		//Debug
	 output reg ARD_RESET=1'b1
);

wire tx_busy;

reg[31:0] ctr;
	
wire ctr_in;

debounce db (.CLK(CLK),
	.noisy(Arduino_2),
	.clean(ctr_in)
);

reg prev_ctrin = 1'b0;
always @(posedge CLK) begin
	if (!prev_ctrin & ctr_in) begin
		ctr = ctr+1;
	end
	prev_ctrin = ctr_in;
	if (Arduino_3) begin
		ctr = 0;
	end
end


shift_transmitter tx(
    .CLK(CLK),
    .data(ctr),
    .tx_start(1'b1),
    .tx_busy(tx_busy),
	 .dataOut(Arduino_10),
	 .dataAdv(Arduino_11),
	 .dataReady(Arduino_12)
);

endmodule
