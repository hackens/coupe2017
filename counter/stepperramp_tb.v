`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:00:06 09/10/2016 
// Design Name: 
// Module Name:    stepperramp_tb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module counter_tb(
	output wire Arduino_13,
  output wire Arduino_10
    );

reg CLK = 0;
always #15.625 CLK=~CLK;
reg exec = 0;

reg din = 0;
reg dadv = 0;


counter DUT(
	 .CLK(CLK),
	 .Arduino_10(Arduino_10),
	 .Arduino_11(Arduino_11),
	 .Arduino_12(Arduino_12),
	 .Arduino_13(Arduino_13)
    );

endmodule
