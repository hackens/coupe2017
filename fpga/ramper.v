`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:18:47 09/11/2016 
// Design Name: 
// Module Name:    ramper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ramper(
    input CLK,
    input start,
    input [15:0] l,	//in approx millimeters (1mm = 4 steps)
 	  input [31:0] ticksDone,
	  output reg [15:0] l_done = 0,
	  input pause,
    output reg [24:0] ticksPS = 0,
	  output reg ramperActive=0,
	  input speed_grade
  );

// invVM = 32000000/(VM en steps/s)
wire[31:0] invVM = speed_grade ? 32000000/5000 : 32000000/2000;	//  tick/step

//	AM = 2000 steps/s^2 = 2000/(32e6)^2 steps/tick/tick
//parameter invAM = 32000000*32000000/2000;	//  tick/step

// AM = VM/(durée de rampe en s)
// vmOverAM = 32000000 * (durée de rampe en s)
wire[31:0] vmOverAm = speed_grade ? 32000000 : 2*32000000;

// inv32MTimesAM = 1/(32000000*AM)
wire[31:0] inv32MTimesAM = speed_grade ? 32000000/5000 : 2*32000000/2000;

reg [31:0] l_copy;

reg prev_start=0;
reg[32:0] timer=0;

reg[32:0] t1=0;
reg[32:0] t2=0;
reg[32:0] t3=0;

reg[2:0] state=0;

reg[32:0] rampTicks = 0;

reg speed_grade_backup = 0;

reg mode=0;	//0 = reaches max speed, 1=max spd not reached
always @(posedge CLK) begin
	if (~pause) begin
		if (start && ~prev_start) begin
			speed_grade_backup = speed_grade;
			prev_start=1;
			l_copy=l*4;
			t1=vmOverAm;
			t2=invVM*l_copy;
			mode = (t2<t1);
			t3=t1+t2;
			ramperActive=1;
		end
		if (~start && prev_start) begin
			timer=0;
			prev_start=0;
			ramperActive=0;
			state=0;
			ticksPS=0;	//causes ticksDone to reset
		end
		if (start & ramperActive) begin
			l_done = ticksDone/4;
			timer = timer+1;
			
      //$display("1 %b %d %d",mode,state,timer);
			
			if (mode) begin	//Count-based
				if (state==0) begin
					ticksPS=timer/inv32MTimesAM+100;
				end
				if (state==2) begin
					if (t3<timer) begin
						ticksPS=100;
					end
					else begin
						ticksPS=(t3-timer)/inv32MTimesAM+100;
					end
				end
				if (state==0 & ticksDone>l_copy/2) begin
					state=2;
					t3=timer*2;
				end
				if (state==2 & ticksDone>=l_copy) begin
					state=0;
					ticksPS=0;
					ramperActive=0;
				end
			end
			else begin		//Timing-based
				if (state==0) begin
					ticksPS=timer/inv32MTimesAM;
				end
				if (state==1) begin
					ticksPS=32000000/invVM;
				end
				if (state==2) begin
					if (t3>timer) begin
						if ((t3-timer)/inv32MTimesAM > 300) begin
							ticksPS=(t3-timer)/inv32MTimesAM;
						end
						else begin
							ticksPS=300;
						end
					end
					else begin
						ticksPS=300;
					end
				end
				if (timer>t1 & state==0) begin
					state=1;
					rampTicks = ticksDone;
				end
				if (ticksDone >= l_copy-rampTicks & state==1) begin
					t3 = timer + t1;
					state=2;
				end
				if (ticksDone >= l_copy & state==2) begin
					state=0;
					ticksPS=0;
					ramperActive=0;
				end
			end
		end
	end
end

endmodule
