`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:00:06 09/10/2016 
// Design Name: 
// Module Name:    stepperramp_tb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module stepperramp_tb(
	output wire Arduino_13,
  output wire Arduino_11
    );

reg CLK = 0;
always #15.625 CLK=~CLK;
reg exec = 0;

reg din = 0;
reg dadv = 0;
reg abort=0;

initial begin
  #5000000
  
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  
  #50000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  
  #50000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=1; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 din=0; #10000 dadv=1; #10000 dadv=0;
  #10000 exec=1; #50000 exec=0;
 // #50000000 abort=1; #10000 abort=0;
end

wire step1, dir1, step2, dir2;

reg ignore_radar = 1;
reg abort_on_radar = 1;

//debugCounter CNT(
//	.CLK(CLK),
//	.in(Arduino_13)
//	);

reg sonarEcho = 0;

always @(posedge CLK) begin
//	#100000000 sonarEcho = 1; #100000 sonarEcho=0;
end

stepperRamp DUT(
	 .CLK(CLK),
	 .Arduino_11(Arduino_11),	//AVR->FPGA ready
	 //AVR->FPGA bus
    .Arduino_2(din),
	 .Arduino_1(1'b1),
    .Arduino_3(dadv),
    .Arduino_4(exec),
    .Arduino_8(abort),
    .Arduino_7(ignore_radar),
    .Arduino_10(abort_on_radar),
	 .Arduino_23(sonarPing),
	 .Arduino_22(sonarEcho),
	 .Arduino_18(Arduino_13),
	 .Arduino_19( dir1),
	 .Arduino_20(step2),
	 .Arduino_21( dir2)
    );

endmodule
