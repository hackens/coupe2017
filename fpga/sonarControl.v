`timescale 1ns / 1ps

module sonarControl(
    input CLK,
    input sonarEcho,
    output reg sonarPing,
    output wire obstacle
    );

reg pingState = 0;
reg [0:32] ctr = 0;

reg [0:16] frame = 0;

// 18560 ticks/m
// 1856  ticks/cm (already accounting for scale factor)
reg [15:0] distance = 60000;

wire notObstacle;
assign obstacle = ~notObstacle;

schmitt #(.LOW(18560),.HIGH(27840)) s (.CLK(CLK), .in(distance), .out(notObstacle));

// echo stays high for a time proportional to the ping duration (factor .1)
always @(posedge CLK) begin
	sonarPing = frame[0];
	frame<=frame+1;
	
	// echo just went high
	if (~pingState & sonarEcho) begin
		pingState <=1;
		ctr<=0;
	end
	
	// echo just went low
	if (pingState & ~sonarEcho) begin
		distance <= ctr;
		ctr<=0;
		pingState<=0;
	end else begin
	
		// echo is high
		if (pingState) begin
			ctr <= ctr+1;
		end
		
		// echo is high but we're out of time
		if (pingState & ctr>=40000) begin
			distance <= 60000;
			ctr<=40000;
		end
	end
end

endmodule
