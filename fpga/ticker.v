`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:19:14 09/11/2016 
// Design Name: 
// Module Name:    ticker 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ticker(
    input CLK,
    input [WIDTH-1:0] ticksPS,
  	input pause,
  	output reg out=0,
  	output reg [31:0] ticksDone=0
  );

parameter WIDTH=25;
parameter DOWNSAMPLE=10;

wire [WIDTH-DOWNSAMPLE-1:0] cyclesPS = 32000000>>DOWNSAMPLE;
wire [WIDTH-DOWNSAMPLE-1:0] cyclesPTickDS;

divider #(WIDTH-DOWNSAMPLE) divider (
  .A(cyclesPS),
  .B(ticksPS),
  .Res(cyclesPTickDS));

wire [WIDTH:0] cyclesPTick = cyclesPTickDS << DOWNSAMPLE;

//always @(posedge CLK)
//  $display("%b %d %d %d %d",pause,ticksPS,ticksDone,ctr,cyclesPTick);

reg [WIDTH-1:0] ctr = 0;
always @(posedge CLK) begin
	if (~pause) begin
		if (ticksPS==0) begin
			out<=0;
			ticksDone<=0;
		end
		else begin
			ctr <= ctr+1;
			out<=(ctr<4000);
			
			if (ctr==5000) begin
				ticksDone <= ticksDone + 1;
			end
			
			if (ctr>=cyclesPTick) begin
				ctr <= 0;
			end
		end
	end
end

endmodule
