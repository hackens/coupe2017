`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:06:10 09/10/2016 
// Design Name: 
// Module Name:    blink_tb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module blink_tb(
    output Arduino_13,
    output CLK_out
    );

reg CLK=0;
assign CLK_out=CLK;

blink DUT(
	.CLK(CLK),
	.Arduino_13(Arduino_13)
);

always #15.625 CLK=~CLK;

endmodule
