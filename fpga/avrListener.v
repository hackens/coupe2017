`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:48:10 09/10/2016 
// Design Name: 
// Module Name:    avrListener 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module avrListener(
    input CLK,
	 input Arduino_10,	//AVR->FPGA ready
    input Arduino_14,
    input Arduino_15,
    input Arduino_16,
    input Arduino_17,
    input Arduino_18,
    input Arduino_19,
    input Arduino_20,
    input Arduino_21,
    output reg [7:0] out_l,
	 output reg output_ready=0
    );


wire [7:0] ardBus;
assign ardBus={Arduino_14,Arduino_15,Arduino_16,Arduino_17,Arduino_18,Arduino_19,Arduino_20,Arduino_21};

reg a10_old = 0;
always @(posedge CLK) begin
	if (Arduino_10 & ~a10_old) begin
		a10_old=1;
		out_l = ardBus;
		output_ready = 1;
	end
	if (~Arduino_10 & a10_old) begin
		a10_old=0;
	end
end

endmodule
