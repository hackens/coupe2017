module debounce ( CLK, noisy, clean);
   input CLK, noisy;
   output clean;

   parameter NDELAY = 1600;
   parameter NBITS = 16;

   reg [NBITS-1:0] count=0;
   reg xnew=0, clean=0;

   always @(posedge CLK) begin
     if (noisy != xnew) begin xnew <= noisy; count <= 0; end
     else if (count == NDELAY) clean <= xnew;
     else count <= count+1;
	end
	
endmodule
