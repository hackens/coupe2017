`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:46:29 05/22/2017 
// Design Name: 
// Module Name:    debugCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module debugCounter(
    input CLK,
    input in,
    output reg [31:0] out = 0
    );

reg latch = 0;

always @(posedge CLK) begin
	if (~latch & in) begin
		out <= out + 32'b1;
		latch <= 1'b1;
	end
	else if (latch & ~in) begin
		latch <= 1'b0;
	end
end

endmodule
