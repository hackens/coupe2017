`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:52:57 09/10/2016 
// Design Name: 
// Module Name:    repeat_avr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module repeat_avr(
    input CLK,
    input Arduino_12,
	 output Arduino_13,
    //output Arduino_13,
	 output ARD_RESET
    );

reg r=1;

assign Arduino_13 = Arduino_12;
assign ARD_RESET=1;


endmodule
