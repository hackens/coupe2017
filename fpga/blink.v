`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:24:08 09/09/2016 
// Design Name: 
// Module Name:    blink 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module blink(
    input CLK,
    output Arduino_13,
	 output ARD_RESET
    );


assign ARD_RESET = 0;

parameter WIDTH=32;
reg sw = 0;


wire [WIDTH-1:0] N = 32000000;
reg [WIDTH-1:0] D = 5;
wire [WIDTH-1:0] c;
divider #(WIDTH) divider (.A(N),.B(D),.Res(c));

assign Arduino_13 = sw;

reg [32:0] counter = 0;
always @(posedge CLK) begin
	counter <= counter + 1;
	if (counter>=c) begin
		counter <= 0;
		sw <= ~sw;
	end
end

reg [32:0] ticks = 0;
always @(posedge CLK) begin
	ticks <= ticks + 1;
	if (ticks>=16000000) begin
		ticks <= 0;
		D <= D+1;
	end
end


endmodule
