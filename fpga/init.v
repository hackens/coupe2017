`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:17:59 09/11/2016 
// Design Name: 
// Module Name:    init 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module init(
    input CLK,
    input avrReady,
	  input ramperActive,
    output reg ARD_RESET=0,
    output reg fpgaReady=0,
    output reg start=0
    );

reg [25:0] timeSincePowerUp = 0;

//Run only every 2 clock ticks, to let other systems update their status first
always @(posedge CLK) begin
	timeSincePowerUp <= timeSincePowerUp+1;
end

reg avrReadyWentDown = 1;

always @(posedge timeSincePowerUp[4]) begin
	if (timeSincePowerUp >= 32000 && ARD_RESET==0) begin		//1ms
		ARD_RESET=1;
		fpgaReady=1;
	end
	
   if (start & ~ramperActive) begin
		fpgaReady=1;
		start=0;
		avrReadyWentDown=0;
	end
	if (~avrReadyWentDown & ~avrReady) begin
		avrReadyWentDown=1;
	end
	else if (fpgaReady & avrReady & avrReadyWentDown & timeSincePowerUp>=64000) begin	//2ms
		start=1;
		fpgaReady=0;
	end
end

endmodule
