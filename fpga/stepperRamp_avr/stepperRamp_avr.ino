const int fpga_avr_ready = 11;
const int data_in = 2;
const int data_advance = 3;
const int data_adv_ack = 9;
const int exec = 4;
const int ignore_radar = 7;
const int abort_on_radar = 10;
const int aborted_because_radar = 12;
const int fpga_abort = 8;
const int sr_out = 5;
const int speed_grade = 1;

unsigned long long until;

/*void while_timeout(int pin, int eq, unsigned long timeoutUS) {
  unsigned long t=micros();
  while (digitalRead(pin)!=eq && micros()<t+timeoutUS);
}
void while_notimeout(int pin, int eq) {
  while (digitalRead(pin)!=eq);
}*/

// Goes forward and waits for completion
String forward(float mm) {
  String s = forward_async(mm);
  if (s.startsWith("error"))
    return s;
  
  while(!digitalRead(fpga_avr_ready) && until > (unsigned long long)millis()) {
    delayMicroseconds(100);
  }
  
  if(until < (unsigned long long)millis()) {
    abort();
  }
  
  delayMicroseconds(100);
  
  if (digitalRead(aborted_because_radar)) {
    int done = 0;
    for (int i=15; i>=0; i--) {
      delayMicroseconds(200);
      digitalWrite(data_advance,1);
      while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis()) ;
      if(until  < (unsigned long long)millis()) {
        abort();
      }
      digitalWrite(data_advance,0);
      delayMicroseconds(200);
      done = 2*done + digitalRead(sr_out);
      delayMicroseconds(200);
    }
    String s2 = "radar : "+String(done);
    return s2;
  }
  
  return s;
}


// Initialize the timer (when the game start)
String init_timer(int mm) {
  until = (unsigned long long)mm * 1000LL + (unsigned long long)millis();
  return "Done";
}


// Goes forward and waits for completion
String forward_async(float mm) {
  if (mm>5000)
    return "error : Maximum 5000";
  
  while(!digitalRead(fpga_avr_ready) && until > (unsigned long long)millis()) {
    delayMicroseconds(100);
  }
  
  if(until < (unsigned long long)millis()) {
    abort();
  }
  
  digitalWrite(exec,0);
  
  delayMicroseconds(1000);
  
  uint16_t data = mm;
  
  String s;
  
  for (int i=15; i>=0; i--) {
    digitalWrite(data_in,(data>>i)%2);
    delayMicroseconds(200);
    digitalWrite(data_advance,1);
    while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis());
    if(until < (unsigned long long)millis()) {
      abort();
    }
    digitalWrite(data_advance,0);
    s=s+digitalRead(sr_out);
    delayMicroseconds(200);
  }
  
  s=s+" ";
  
  for (int i=15; i>=0; i--) {
    digitalWrite(data_in,(data>>i)%2);
    delayMicroseconds(200);
    digitalWrite(data_advance,1);
    while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis());
    if(until < (unsigned long long)millis()) {
      abort();
    }
    digitalWrite(data_advance,0);
    s=s+digitalRead(sr_out);
    delayMicroseconds(200);
//    data >>= 1;
  }
  
  s=s+" ";
  
  for (int i=15; i>=0; i--) {
    digitalWrite(data_in,(data>>i)%2);
    delayMicroseconds(200);
    digitalWrite(data_advance,1);
    while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis());
    if(until < (unsigned long long)millis()) {
      abort();
    }
    digitalWrite(data_advance,0);
    s=s+digitalRead(sr_out);
    delayMicroseconds(200);
//    data >>= 1;
  }
  
  Serial.print("start ");
  Serial.println(data);
  digitalWrite(exec,1);
  delayMicroseconds(1000);
  digitalWrite(exec,0);
  if (digitalRead(6)) {
    return "error : FPGA returned error status "+s;
  }
  return "done : all "+s;
}

String forward_async_isdone() {
  if (digitalRead(fpga_avr_ready)) {
    delayMicroseconds(100);
    
    if (digitalRead(aborted_because_radar)) {
      int done = 0;
      for (int i=15; i>=0; i--) {
        delayMicroseconds(200);
        digitalWrite(data_advance,1);
        while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis());
        if(until < (unsigned long long)millis()) {
          abort();
        }
        digitalWrite(data_advance,0);
        delayMicroseconds(200);
        done = 2*done + digitalRead(sr_out);
        delayMicroseconds(200);
      }
      String s = "radar : "+String(done);
      return s;
    }
    
    return "done : async finished";
  }
  return "async : done";
}

int forward_async_abort() {
  digitalWrite(fpga_abort,1);
  delayMicroseconds(100);
  digitalWrite(exec,0);
  delayMicroseconds(100);
  digitalWrite(fpga_abort,0);
  delayMicroseconds(100);
  
  int done = 0;
  for (int i=15; i>=0; i--) {
    delayMicroseconds(200);
    digitalWrite(data_advance,1);
    while (!digitalRead(data_adv_ack) && until > (unsigned long long)millis());
    if(until < (unsigned long long)millis()) {
      abort();
    }
    digitalWrite(data_advance,0);
    delayMicroseconds(200);
    done = 2*done + digitalRead(sr_out);
    Serial.println(digitalRead(sr_out));
    delayMicroseconds(200);
  }
  
  Serial.println(done);
  return done;
}

void setup() {
  for (int i=0; i<=13; i++)
    pinMode(i,INPUT);
  
  Serial.begin(9600);
  
  pinMode(speed_grade,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(11,INPUT);
  pinMode(ignore_radar,OUTPUT);
  pinMode(abort_on_radar,OUTPUT);
  pinMode(sr_out,INPUT);
  pinMode(data_adv_ack,INPUT);
  pinMode(aborted_because_radar,INPUT);
  pinMode(fpga_abort,OUTPUT);
  digitalWrite(ignore_radar,0);
  digitalWrite(abort_on_radar,0);
  digitalWrite(speed_grade,0);
  until = (unsigned long long)-1;
}


void abort() {
  digitalWrite(fpga_abort,1);
  Serial.println("TIMEOUT");
}


String serialBuffer;

String getSerialLine() {
  unsigned long i=0;
  while (true) {
    while (Serial.available()>0) {
      char c = Serial.read();
      if (c=='\n' || c=='\r') {
        String copy=serialBuffer;
        serialBuffer="";
        return copy;
      }
      serialBuffer.concat(c);
      i=0;
    }
    if(until < (unsigned long)millis()) {
      abort();
    }
    delayMicroseconds(10);
    i++;
  }
  return "";
}

void loop() {
  String l = getSerialLine();
  if (l.startsWith("FWD  ")) {
    float len = l.substring(5).toFloat();
    String r = forward(len);
    if (!r.startsWith("error"))
      Serial.println(r);
    else
      Serial.println(r);
  }
  else if (l.startsWith("FWD ")) {
    float len = l.substring(4).toFloat();
    String r = forward(len);
    if (!r.startsWith("error"))
      Serial.println(r);
    else
      Serial.println(r);
  }
  else if (l.startsWith("ASYNC ")) {
    float len = l.substring(6).toFloat();
    String r = forward_async(len);
    if (!r.startsWith("error"))
      Serial.println(r);
    else
      Serial.println(r);
  }
  else if (l == "DONE?") {
    String r = forward_async_isdone();
    Serial.println(r);
  }
  else if (l == "ABORT") {
    int done = forward_async_abort();
    String s = String(done);
    Serial.println("done : "+s);
  }
  else if (l.startsWith("RADAR")) {
    Serial.println(digitalRead(13));
  }
  else if (l.startsWith("IGNORE_RADAR")) {
    Serial.println("Ignoring radar");
    digitalWrite(ignore_radar,1);
    digitalWrite(abort_on_radar,0);
  }
  else if (l.startsWith("UNIGNORE_RADAR")) {
    Serial.println("Ungnoring radar");
    digitalWrite(ignore_radar,0);
    digitalWrite(abort_on_radar,0);
  }
  else if (l.startsWith("ABORT_ON_RADAR")) {
    Serial.println("Aborting on radar");
    digitalWrite(abort_on_radar,1);
    digitalWrite(ignore_radar,1);
  }
  else if (l.startsWith("NO_ABORT_ON_RADAR")) {
    Serial.println("Not aborting on radar");
    digitalWrite(abort_on_radar,0);
    digitalWrite(ignore_radar,0);
  }
  else if (l.startsWith("FAST")) {
    Serial.println("Speed grade 1");
    digitalWrite(speed_grade,1);
  }
  else if (l.startsWith("SLOW")) {
    Serial.println("Speed grade 0");
    digitalWrite(speed_grade,0);
  }
  else if (l.startsWith("TIMER ")) {  //The argument is in seconds
    float len = l.substring(6).toInt();
    String r = init_timer(len);
    Serial.println(r);
  }
  else if (l.startsWith("RESET")) {
    Serial.println("Abort signal reseted");
    digitalWrite(fpga_abort,0);
    until = (unsigned long) -1;
  }
  else if (l.length()>0) {
    Serial.print("Unrecognized command ");
    Serial.println(l);
    Serial.println("done");
  }
  else {
    Serial.println("Empty command");
    Serial.println("done");
  }
}

