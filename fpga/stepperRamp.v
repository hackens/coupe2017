`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:52:55 09/10/2016 
// Design Name: 
// Module Name:    stepperRamp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module stepperRamp(
    input CLK,
	  output Arduino_11,	//FPGA->AVR ready
	 
	  //AVR->FPGA bus
    input Arduino_2,  // Data in
    input Arduino_3,  // Data advance
    input Arduino_4,  // Exec
	 input Arduino_7,	 // Ignore radar
	 input Arduino_8,	 // Abort
	 output reg Arduino_9=0,	//data_adv_ack
	 input Arduino_10,	 // Abort on radar
	 output reg Arduino_12=0,	//aborted because radar
	 input Arduino_1,	//speed grade
	 
	 //To abort, drive abort high, then exec low (if not already low), then abort low
	 
    output wire Arduino_18,	//Step 1
	  output wire Arduino_19,	//Dir 1
	  output wire Arduino_20,	//Step 2
	  output wire Arduino_21,	//Dir 2

	  output reg Arduino_5,	//Debug out 1 (shift reg out)
	  output reg Arduino_6=1'b0,	//Debug out 2 (ERROR)
	 
	  input  Arduino_22, //Echo
	  output Arduino_23,	//Trig
	 
	  //Debug ports
	  output wire Arduino_13,
	  //output Arduino_24,
	  output ARD_RESET
    );

wire dataIn, dataAdvance, execIn;
//debounce d1(CLK,Arduino_2,dataIn);
//debounce d2(CLK,Arduino_3,dataAdvance);
//debounce d3(CLK,Arduino_4,execIn);

assign dataIn = Arduino_2;
assign dataAdvance = Arduino_3;
assign execIn = Arduino_4;

//Set step and dir pins
assign Arduino_19 = 0;
assign Arduino_20 = Arduino_18;
assign Arduino_21 = 0;

wire obstacle;
assign Arduino_13 = obstacle;

wire abort = Arduino_8;
wire abortOnRadar = Arduino_10;
wire ignoreRadar = Arduino_7;
wire speed_grade = Arduino_1;

reg [47:0] shiftRegister = 47'b0;
reg prevDataAdvance = 0;
reg prevExec = 0;
reg exec=0;
reg [5:0] clk_ds = 0;

wire [15:0] l_done;
reg [15:0] l_done_out;

always @(posedge CLK) begin
  clk_ds <= clk_ds + 1;
end

reg [15:0] regDistanceMm = 0;

reg output_l_done = 0;

wire [15:0] v1 = shiftRegister[15:0];
wire [15:0] v2 = ~shiftRegister[31:16];
wire [15:0] v3 = shiftRegister[47:32];

reg internal_abort=0;

always @(posedge clk_ds[3]) begin
  if (abort) begin
	 if (~output_l_done) begin
		l_done_out = l_done;
	 end
	 output_l_done = 1;
	 internal_abort=1;
  end
  else if (abortOnRadar && obstacle) begin
	 if (~output_l_done) begin
		l_done_out = l_done;
	 end
	 output_l_done = 1;
	 internal_abort=1;
	 Arduino_12 = 1;
  end
  else begin
	 internal_abort=0;
  end
  
  if (!prevDataAdvance && dataAdvance) begin
    Arduino_12 = 0;
    prevDataAdvance <= 1;
	 if (output_l_done) begin
		Arduino_5 = l_done_out[15];
		l_done_out = {l_done_out[14:0],l_done_out[15]};
	 end
	 else begin
		Arduino_5 = shiftRegister[47];
	 end
    shiftRegister <= {shiftRegister[46:0], dataIn};
	 Arduino_9 <= 1;
  end
  if (prevDataAdvance && !dataAdvance) begin
    prevDataAdvance <= 0;
	 Arduino_9 <= 0;
  end
  
  if (!prevExec && execIn) begin
	 output_l_done = 0;
	 if (v1==v2) begin
		regDistanceMm = v1;
		prevExec <= 1;
		exec = 1;
		Arduino_6 <= 0;
	 end
	 else if (v2==v3) begin
		regDistanceMm = v3;
		prevExec <= 1;
		exec = 1;
		Arduino_6 <= 0;
	 end
	 else if (v1==v3) begin
		regDistanceMm = v3;
		prevExec <= 1;
		exec = 1;
		Arduino_6 <= 0;
	 end
	 else begin
		Arduino_6 <= 1;
	 end
  end
  if (prevExec && !execIn) begin
    prevExec <= 0;
    exec=0;
	 Arduino_6 <= 0;
  end
end

wire start;
wire [24:0] ticksPS;
wire ramperActive;
wire [31:0] ticksDone;

wire pause = obstacle && (! Arduino_7);

init init(.CLK(CLK),.avrReady(exec),.ramperActive(ramperActive),
	.ARD_RESET(ARD_RESET), .fpgaReady(Arduino_11), .start(start));

// Wait for output_l_done to raise before stopping ramper
ramper ramper(.CLK(CLK), .start(start && !(internal_abort && output_l_done)), .l(regDistanceMm), .ticksDone(ticksDone), .pause(pause),
	.ticksPS(ticksPS),.ramperActive(ramperActive),.l_done(l_done),.speed_grade(speed_grade));
	
ticker ticker(.CLK(CLK), .ticksPS(ticksPS), .pause(pause),
	.out(Arduino_18 /**debug**/),.ticksDone(ticksDone));
	
sonarControl sonar(.CLK(CLK), .sonarEcho(Arduino_22), .sonarPing(Arduino_23), 
	.obstacle(obstacle));
	
// debounce #(.NDELAY(800000),.NBITS(23)) obstacleDebouncer(CLK,obstacle,obstacleDB);

endmodule
