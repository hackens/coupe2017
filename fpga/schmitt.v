`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:27:27 05/22/2017 
// Design Name: 
// Module Name:    schmitt 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module schmitt(
	 input CLK,
    input [15:0] in,
    output reg out = 0
    );
	 
parameter[32:0] HIGH = 15;
parameter[32:0] LOW = 8;

always @(posedge CLK) begin
	if (out && (in<LOW)) begin
		out = 0;
	end
	if (!out && (in>HIGH)) begin
		out = 1;
	end
end


endmodule
