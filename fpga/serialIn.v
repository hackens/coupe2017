`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:29:10 01/11/2017 
// Design Name: 
// Module Name:    serialIn 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module serialIn(
    input CLK,
    input [0:3] ardBus,
    output [0:64] packetOut,
    output outReady,
    input inReady
    );


endmodule
