#pragma once
#include <set>

#include <string>

#include "angle.h"
#include "vector.h"

/** \brief Genaral namespace for all Board-related element.*/
namespace Board {
  enum class Color {Blue, Yellow};

  /** \brief Generic class for Cylinders, Pool of Balls, initial tubes, ...*/
  class BoardElement  {
    public :
    	BoardElement(std::string name = "");

    	virtual std::string getDebugType() = 0;

    	Vector2 position;

    	/// In order to initialize both side at the same time
    	virtual BoardElement* createSymetic() = 0;

    	/// Maybe useful for smarter AI or just debug info.
    	//virtual int points() =0;

    	std::string getName();

    	BoardElement* mSymmetric;

    private :
	    std::string mName;
  };

  /// \brief Just a subclass of a set of FieldElement with some handy methods (Constructor)
  class Board {
    public:
	    /** \brief Instanciate all the Element on the field : 
	    	This is the place where to add or remove element if the rules change again.
	    	The first param is currently ignored but may be used if needed
       */
	    Board();

	    ~Board() = delete;

	    ///May be useful of not, currently not implemented
	    BoardElement* getByName(std::string name);

      void removeElement(BoardElement* e);

    private:
       std::set<BoardElement*> mElements;
  };

  class MonoCylinder : public BoardElement {
    public:
	    MonoCylinder(Color cl);
	    Color mColor;
  };

  class BiCylinder : public BoardElement {
    public:
	    BiCylinder(Angle side);

	    /** \brief The orientation of the cylinder
	    	\todo Choose an orientantion convention
	     */
	    Angle mSide;
  };
};

