#pragma once

#include <iostream>
#include <iomanip>
#include <time.h>

/// Contain all logging methods and class
namespace Logging {
  /** \brief Standard logger
	  
	  It gives a << operator but flush at each operation
  */
  class Logger {
    public:
      Logger() {
      }

      template<typename T>
      const Logger& operator<<(const T& v) const {
        log(v);
        return *this;
      }

      template <typename T>
      void log(const T& v) const {
        std::cout << v;
        std::cout << std::flush;
      }

      static clock_t sProgramStart;

      static double millis() {
        double diffticks=clock()-sProgramStart;
        double diffms=(diffticks)/(CLOCKS_PER_SEC/1000);
        return diffms;
      }
  };


  class ErrorLogger : public Logger {
    public:
      template<typename T>
      const Logger& operator<<(const T& v) const {
        log(v);
        return *this;
      }
      template <typename T>
      void log(const T& v) const {
        std::cerr << "[EE] "<<millis()<<" "<<v;
        std::cerr << std::flush;
      }
  };

  class InfoLogger : public Logger {
    public:
      template<typename T>
      const Logger& operator<<(const T& v) const {
        log(v);
        return *this;
      }
      template <typename T>
      void log(const T& v) const {
        std::cout << "[II] "<< std::setw(7) << std::fixed << std::setprecision(2)
                  << millis()<<" "<<v;
        std::cout << std::flush;
      }
  };

  class DebugLogger : public Logger {
    public:
      template<typename T>
      const Logger& operator<<(const T& v) const {
        log(v);
        return *this;
      }
      template <typename T>
      void log(const T& v) const {
        std::cout << "[DD] "<<millis()<<" "<<v;
        std::cout << std::flush;
      }
  };

  extern ErrorLogger error;
  extern InfoLogger info;
  extern DebugLogger debug;
};

