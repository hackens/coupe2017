#include "taskPrim.h"
#include "robot.h"
using namespace std;

namespace Behavior {
  TaskPrim::TaskPrim(string name) : mName(name)
  {
  }

  MotionTask::MotionTask(Mobility::Motion motion,string name) : TaskPrim(name),mMotion(motion){}

  void MotionTask::execute(){
    mMotion.execute();
  }

}
