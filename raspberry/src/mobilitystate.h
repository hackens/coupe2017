#pragma once

#include "distance.h"
#include "angle.h"
#include "motion.h"
#include <iostream>

namespace Mobility {
  /// Contain a position on the map 
  class MobilityState {
    public:
      MobilityState();
      MobilityState(Distance x, Distance y, Angle h);

      Distance x() const noexcept;
      Distance y() const noexcept;
      Angle h() const noexcept;

      const MobilityState& operator+=(const Motion& m);

    protected:
      Distance mX=0, mY=0;
      Angle mH=0;
  };

  inline std::ostream& operator<<(std::ostream& s, const MobilityState& t) {
    s << "{x=" << t.x().meters() << ", y="<< t.y().meters() << ", mH=" << t.h() << "} ";
    return s;
  }
};

