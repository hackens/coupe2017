#include "robot.h"
#include "dummy_steppers.h"
#include "dummy_servos.h"
#include "time.h"

Com::Serial Robot::sStepper("/dev/ttyACM0",9600, new Com::DummySteppers());
Com::Serial Robot::sServos("/dev/ttyUSB0",9600, new Com::DummyServos());
Action Robot::sAction(&sServos);
Gpio Robot::sDirLeft(20,Gpio::OUT);
Gpio Robot::sDirRight(21,Gpio::OUT);
Gpio Robot::sRunLed(13,Gpio::OUT);
Gpio Robot::sErrorLed(19,Gpio::OUT);
Vector2 Robot::sPosition(18_cm,89_cm);
Angle Robot::sOrientation = 0;
Graph Robot::sGraph;
bool Robot::sYellow = false;
time_t Robot::sStart = 0;
int Robot::sTime = 5;

