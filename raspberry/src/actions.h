#pragma once
#include "gpio.h"
#include "com.h"

class Action {
  public:
    Action(Com::Serial* arduino);

    void grab();
    void grabSeq();
    void grabAllSeq();

    void release();
    void releaseSeq();
    void releaseAllSeq();

    void beltStraight();
    void beltPull();
    void beltStop();
    void beltPush();

    void lock();
    void unlock();

    void lift();
    void up();
    void lower();

    void detachAll();

    void launch_rocket();
    void lock_rocket();
    
    enum Sensor{
      FRONT = 1, BACK, UP, START
    };

    bool getSensor(Sensor s);

  private:
    void detachServo(int s);
    Com::Serial* mArduino;
};
