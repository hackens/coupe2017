#include "dummy_steppers.h"

using namespace Com;

std::string DummySteppers::readLine(bool wait) {
  if (mState==0) {
    if (wait) {
      std::cerr<<"Warning : infinite loop in dummy_steppers !"<<std::endl;
      while (true);
    }
    else
      return std::string();
  }
  else if (mState==1) {
    mState=2;
    return std::string("start -42");
  }
  else if (mState==2) {
    mState=0;
    return std::string("done: ok");
  }
  return "";
}

std::string DummySteppers::readLine(int) {
  return readLine(false);
}

void DummySteppers::flush() {
  mState=0;
}

void DummySteppers::writeMessage(std::string) {
  mState=1;
}


