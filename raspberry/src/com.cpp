#include "com.h"
#include "logging.h"
#include <fcntl.h>
#include <unistd.h>
#include <sstream>
#include <sys/time.h>
#include <stdexcept>
#include <sstream>

using namespace Com;
using namespace Logging;

Serial::Serial(std::string device, int baudrate, DummySerial* dummy) {
  if (device=="/dev/ttyACM0") {
    for (int i=0; i<9; ++i) {
      std::stringstream ss;
      ss<<"/dev/ttyACM"<<i;
      std::string dev = ss.str();

      std::cout<<"Trying to connect to "<<dev<<std::endl;
      init(dev,baudrate,dummy);
      if (!mDummy)
        break;
    }
  }
  else if (device=="/dev/ttyUSB0") {
    for (int i=0; i<9; ++i) {
      std::stringstream ss;
      ss<<"/dev/ttyUSB"<<i;
      std::string dev = ss.str();

      std::cout<<"Trying to connect to "<<dev<<std::endl;
      init(dev,baudrate,dummy);
      if (!mDummy)
        break;
    }
  }
  else {
    init(device,baudrate,dummy);
  }
}

void Serial::init(std::string device, int baudrate, DummySerial* dummy) {
  mDevice=device;
  mFd=0;
  mDummy=dummy;
  try {
    // Open interface
    mFd = open(mDevice.c_str(), O_RDWR|O_NOCTTY|O_NDELAY|O_NONBLOCK);
    if (mFd==-1) {
      std::stringstream ss;
      ss<<"Fatal error in ctor Serial : could not open device "<<device;
      throw std::runtime_error(ss.str());
    }

    // Configure interface
    if(!isatty(mFd)) {
      std::stringstream ss;
      ss<<"Fatal error in ctor Serial : could open device "<<device<<" but is not a tty";
      close(mFd);
      throw std::runtime_error(ss.str());
    }

    struct termios config;
    if(tcgetattr(mFd, &config) < 0) {
      std::stringstream ss;
      ss<<"Fatal error in ctor Serial : could open device "<<device<<" and is a TTY, but can't read its configuration";
      close(mFd);
      throw std::runtime_error(ss.str());
    }


    /* Input flags - Turn off input processing
       convert break to null byte, no CR to NL translation,
       no NL to CR translation, don't mark parity errors or breaks
       no input parity check, don't strip high bit off,
       no XON/XOFF software flow control */
    config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                        INLCR | PARMRK | INPCK | ISTRIP | IXON);
 
    /* Output flags - Turn off output processing
       no CR to NL translation, no NL to CR-NL translation,
       no NL to CR translation, no column 0 CR suppression,
       no Ctrl-D suppression, no fill characters, no case mapping,
       no local output processing */
    config.c_oflag  = 0;
 
    /* No line processing
       echo off, echo newline off, canonical mode off, 
       extended input processing off, signal chars off */
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
 
    /* Turn off character processing
       clear current char size mask, no parity checking,
       no output processing, force 8 bit input */
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;
 
    /* One input byte is enough to return from read()
       Inter-character timer off */
    config.c_cc[VMIN]  = 1;
    config.c_cc[VTIME] = 0;
 
    /* Communication speed (simple version, using the predefined
       constants)*/
    if (baudrate != 9600) {
      close(mFd);
      throw std::string("Baud rates other than 9600 are "
        "not implemented yet");
    }
    if(cfsetispeed(&config,B9600)<0 || cfsetospeed(&config,B9600)<0) {
      std::stringstream ss;
      ss<<"Fatal error in ctor Serial : could open device "<<device<<" and is a TTY, but can't change its baudrate to 9600";
      close(mFd);
      throw std::runtime_error(ss.str());
    }

    if(tcsetattr(mFd, TCSAFLUSH, &config) < 0) {
      std::stringstream ss;
      ss<<"Fatal error in ctor Serial : could open device "<<device<<" and is a TTY, but can't change its configuration";
      close(mFd);
      throw std::runtime_error(ss.str());
    }

    if (mDummy) {
      delete mDummy;
      mDummy=nullptr;
    }
  }
  catch (std::runtime_error e) {
    std::cerr<<e.what()<<std::endl;
    if (mDummy) {
      std::cerr<<"Continuing with dummy device"<<std::endl;
    }
    else {
      std::cerr<<"No dummy device detected, throwing again"<<std::endl;
      throw e;
    }
  }
  std::cout<<"Opened "<<device<<std::endl;
}

Serial::~Serial() {
  close(mFd);
  mFd=0;
  if (mDummy)
    delete mDummy;
}

std::string Serial::readLine(bool wait) {
  if (mDummy)
    return mDummy->readLine(wait);

  char c;
  while (wait) {
    while (read(mFd,&c,1)>0) {
      if (c=='\n') {
        std::string ret = mBuffer;
        mBuffer.clear();
        return ret;
      }
      if (c!='\r')
        mBuffer += c;
      //Logging::debug << mBuffer;
    }
  }
  return std::string();
}

std::string Serial::readLine(int waitUS) {
  if (mDummy)
    return mDummy->readLine(waitUS);

  struct timespec start, end;

  clock_gettime(CLOCK_REALTIME, &start);

  char c;
  while (true) {
    clock_gettime(CLOCK_REALTIME, &end);
    int elapsed = ( end.tv_sec - start.tv_sec )*1000000
            + ( end.tv_nsec - start.tv_nsec )/1000;
    if (elapsed>waitUS)
      return std::string();

    if (read(mFd,&c,1)>0) {
      if (c=='\n') {
        std::string ret = mBuffer;
        mBuffer.clear();
        return ret;
      }
      if (c!='\r')
        mBuffer += c;
    }
  }
  
  return std::string();
}

Message Serial::readMessage(bool wait) {
  std::string s = readLine(wait);
  return Message(s);
}

Message Serial::readMessage(int waitUS) {
  std::string s = readLine(waitUS);
  return Message(s);
}

void Serial::flush() {
  if (mDummy)
    mDummy->flush();

  char c;
  while (read(mFd,&c,1)>0);
  mBuffer.clear();
}

void Serial::writeMessage(std::string s) const {
  if (mDummy)
    return mDummy->writeMessage(s);

  int size = s.length();
  int offset=0;
  while (size>0) {
    int w = write(mFd,s.substr(offset).c_str(),size);
    //debug<<"wrote "<<w<<" bytes\n";
    if (w<=0) {
      std::stringstream ss;
      ss<<"Could not write message "<<s<<" because: "<<errno;
      throw ss.str();
    }
    size -= w;
    offset += w;
  }
}

