#include "task.h"
#include "robot.h"
#include "motion.h"
#include "taskPrim.h"
#include <cmath>
#include <iostream>
#include "gui_interface.h"
#include "logging.h"
#include <assert.h>
#include <unistd.h>

using namespace std;
using namespace Logging;
using namespace Behavior;

Task::Task(string name) : mName(name), mDone(false)
{
}

DirectGoTo::DirectGoTo(string name,Vector2 dest,Distance epsilon,Angle alpha)
  : Task(move(name)),mDest(dest),mEpsilon(epsilon),mAlpha(alpha){

}

bool DirectGoTo::step(){
  Vector2 movement = mDest - Robot::sPosition;
  Angle aMov = movement.orientation() - Robot::sOrientation;
  info << "DGT going to" << mDest << "from" << Robot::sPosition << "\n";
  debug << "DGT movement : " << movement << "and rotation " << aMov << "\n";
  if(aMov > M_PI) aMov -= 2*M_PI;
  if(aMov < -M_PI) aMov += 2*M_PI;
  if(movement.Norm() < mEpsilon){
    mDone = true;
    return false;
  }
  if (std::abs(aMov) > mAlpha){
    MotionTask mt(Mobility::Motion(0,aMov),"DirectGoTo Rotation");
    mt.execute();
    return true;
  }
  if(movement.Norm() > mEpsilon){
    MotionTask mt(Mobility::Motion(movement.Norm(),0),"DirectGoTo Translation");
    mt.execute();
  }
  mDone = true;
  return false;
}

void DirectGoTo::rearm(Vector2 dest){
  mDone = false;
  mDest = dest;
}


DirectGoToG::DirectGoToG(string name,Vector2 dest,Distance epsilon,Angle alpha)
  : Task(move(name)),mDest(dest),mEpsilon(epsilon),mAlpha(alpha){

}

bool DirectGoToG::step(){
  Vector2 movement = mDest - Robot::sPosition;
  Angle aMov = movement.orientation() - Robot::sOrientation;
  info << "DGT going to" << mDest << "from" << Robot::sPosition << "\n";
  debug << "DGT movement : " << movement << "and rotation " << aMov << "\n";
  if(aMov > M_PI) aMov -= 2*M_PI;
  if(aMov < -M_PI) aMov += 2*M_PI;
  if(movement.Norm() < mEpsilon){
    mDone = true;
    return false;
  }
  if (std::abs(aMov) > mAlpha){
    MotionTask mt(Mobility::Motion(0,aMov),"DirectGoTo Rotation");
    mt.execute();
    return true;
  }
  if(movement.Norm() > mEpsilon){
    Robot::sAction.grab();
    Robot::sAction.beltPull();
//    Mobility::Motion(25_cm,0).execute();
    usleep(3500000);
    Robot::sAction.beltStop();
    Robot::sAction.release();
    usleep(500000);
    Robot::sAction.beltStraight();
    Robot::sAction.beltPull();
    usleep(2000000);
    Robot::sAction.beltStop();
    MotionTask mt(Mobility::Motion(movement.Norm()/*-25_cm*/,0),"DirectGoTo Translation");
    mt.execute();
  }
  mDone = true;
  return false;
}

void DirectGoToG::rearm(Vector2 dest){
  mDone = false;
  mDest = dest;
}

/* ____           _     _____                    _   _
  / ___|_ __ __ _| |__ |  ___| __ ___  _ __ ___ | | | | ___ _ __ ___
 | |  _| '__/ _` | '_ \| |_ | '__/ _ \| '_ ` _ \| |_| |/ _ \ '__/ _ \
 | |_| | | | (_| | |_) |  _|| | | (_) | | | | | |  _  |  __/ | |  __/
  \____|_|  \__,_|_.__/|_|  |_|  \___/|_| |_| |_|_| |_|\___|_|  \___|
*/

const Distance GrabFromHere::minDist = 15_cm;
const Distance GrabFromHere::apprSteps = 5_mm;
GrabFromHere::GrabFromHere(const string& name,Vector2 cylPos, bool rocket, int numCyl)
  : Task(name), mDest(cylPos), mReleased (false), mFailed(false), mRocket(rocket),
    mDepDone(false), mNumCyl(numCyl), mDgt(name + " DGT",cylPos){

  /*if((cylPos - Robot::sPosition).Norm() < minDist){
    mFailed = true;
    }*/
}

Vector2 GrabFromHere::getVec(Vector2& vec){
  Vector2 dep = Robot::sPosition - vec;
  dep.reNorm(minDist);
  debug << "GFH targeting " << vec + dep << "\n";
  return vec + dep;
}

bool GrabFromHere::step(){
  if(!mDepDone){
    mDgt.rearm(getVec(mDest));
    mDepDone = true;
  }
  //if(mRocket){
  Mobility::Motion::ignoreRadar = true;
  //}
  if(!mDgt.done()){
    Robot::sAction.release();
    mDgt.step();
    return true;
  }


  if(!mReleased){
    info << "GFH releasing\n";
    Robot::sAction.release();
    mReleased = true;
    return true;
  }

  info << "GFH Cylinder testing\n";
  if(Robot::sAction.getSensor(Action::FRONT) and mTryNum < 20){
    info << "GFH Approaching\n";
    Mobility::Motion m(apprSteps,0,true);
    m.execute();
    ++mTryNum;
    if(mTryNum == 20){
      mFailed = true;
      mDone = true;
      return false;
    }
    return true;
  }
  info << "GFH Cylinder in front" << "\n";

  // here cylinder is right in front of us.
  if(mRocket){
    Mobility::Motion m(-15_mm,0,true);
    m.execute();
    Robot::sAction.grab();
    Robot::sAction.beltPull();
    usleep(500000);
    Mobility::Motion m2(-8_cm,0,true);
    m2.execute();
    Robot::sAction.grabSeq();
    if(Robot::sAction.getSensor(Action::BACK)){
      info << "GFH failed to grab, abort grabbing" << "\n";
      Robot::sAction.beltStop();
      Robot::sAction.release();
      if(mNumCyl > 1){
        info << "GFH failed and retry\n";
        mNumCyl--;
        Mobility::Motion m(7.5_cm,0,true);
        m.execute();
        mTryNum = 0;
        return true;
      }
      else{
        Mobility::Motion::ignoreRadar = false;
        mDone = true;
        return false;
      }
    }
    if(mNumCyl > 1 and !Robot::sAction.getSensor(Action::BACK)){
      info << "GFH locking cylinder\n";
      Robot::sAction.lock();
      Robot::sAction.unlock();
      Robot::sAction.lift();
      Robot::sAction.lock();
      Robot::sAction.lower();
      Robot::sAction.release();
      Mobility::Motion m(7.5_cm,0,true);
      m.execute();
      mTryNum = 0;
      mNumCyl--;
      return true;
    }
  }
  else {
    info << "Small GFH" << "\n";
    Mobility::Motion m(-15_mm,0,true);
    m.execute();
    Robot::sAction.grab();
    Robot::sAction.beltPull();
    usleep(1000000);
    Mobility::Motion m2(-8_cm,0,true);
    m2.execute();
    Robot::sAction.beltStop();
    // TODO lift;
  }
  Mobility::Motion::ignoreRadar = false;
  mDone = true;
  return false;
}


/*  ____       _         ____                 _
   / ___| ___ | |_ ___  / ___|_ __ __ _ _ __ | |__
  | |  _ / _ \| __/ _ \| |  _| '__/ _` | '_ \| '_ \
  | |_| | (_) | || (_) | |_| | | | (_| | |_) | | | |
   \____|\___/ \__\___/ \____|_|  \__,_| .__/|_| |_|
                                       |_|
*/

GotoGraph::GotoGraph(const string& name, Graph& g, int startVert, int destVert)
  :Task(name), currentDest(startVert), finalDest(destVert), mGraph(g) {
}


bool GotoGraph::step(){
  DirectGoTo dgt(mName + " DGT",mGraph[currentDest].pos());
  if(dgt.step()) return true;
  if(currentDest == finalDest) {
    mDone  = true;
    return false;
  }
  // next vertex;
  vector<Distance> smallest(mGraph.size(),100000000000000.0_m);
  vector<int> parent(mGraph.size(),-1);
  set<int> toUpdate = {finalDest};
  smallest[finalDest] = 0.0_m;
  set<int> toUpdateNext;

  while(!toUpdate.empty()){
    for(auto i : toUpdate){
      if(!mGraph[i].active()) continue;
      //debug << "propagating on " << mGraph[i] << " of size" << mGraph[i].neigh().size() << "\n";
      for(auto neigh : mGraph[i].neigh()){
        if(smallest[neigh] > smallest[i] + mGraph[i].dist(mGraph[neigh])){
          parent[neigh] = i;
          toUpdateNext.insert(neigh);
          smallest[neigh] = smallest[i] + mGraph[i].dist(mGraph[neigh]);
        }
      }
    }
    toUpdate = move(toUpdateNext);
    toUpdateNext.clear();
  }
  currentDest = parent[currentDest];
  debug << "Goto Graph : chosen to go to " << mGraph[currentDest] << "\n";
  if(currentDest == -1){
    error << "GotoGraph failed" << "\n";
    return false;
  }
  return step();
}



/* ____      _                     ____  _
  |  _ \ ___| | ___  __ _ ___  ___|  _ \(_)_ __
  | |_) / _ \ |/ _ \/ _` / __|/ _ \ | | | | '__|
  |  _ <  __/ |  __/ (_| \__ \  __/ |_| | | |
  |_| \_\___|_|\___|\__,_|___/\___|____/|_|_|
*/

ReleaseDir::ReleaseDir(const std::string& name, Vector2 startPos,Angle startOri, bool all)
  : Task(name), mDgt(name + " DGT",startPos), mOrientation(startOri), mAll(all){

}

bool ReleaseDir::step(){
  if(!mDgt.done()){
    mDgt.step();
    return true;
  }

  Angle aMov = mOrientation - Robot::sOrientation;
  if(aMov > M_PI) aMov -= 2*M_PI;
  if(aMov < -M_PI) aMov += 2*M_PI;
  debug << "RD turning from " << Robot::sOrientation << " to "
        << mOrientation <<" of :" << aMov << "\n";
  if(std::abs(aMov) > 0.03){
    Mobility::Motion mt (0,aMov);
    mt.execute();
    return true;
  }
  if(mAll){
    Robot::sAction.releaseAllSeq();
  }
  else{
    //Robot::sAction.releaseSeq();
    Robot::sAction.beltPush();
    Mobility::Motion m (1_cm,0);
    m.execute();
    usleep(7000000);
    Mobility::Motion (-1_cm,0).execute();
    Robot::sAction.beltStop();
    Robot::sAction.release();
    Mobility::Motion m2 (4_cm,0);
    m2.execute();
    Mobility::Motion mt (-5_cm,0);
    mt.execute();
    /*Robot::sAction.beltStraight();
    usleep(2000000);
    Mobility::Motion m3 (2_cm,0);
    m3.execute();*/
    Robot::sAction.release();

  }
  Robot::sAction.release();
  mDone = true;
  return false;
}
