#pragma once

#include "messages.pb.h"
#include "vector.h"
#include "angle.h"
#include <fstream>
#include <memory>

extern std::shared_ptr<std::ofstream> gui_fifo;

void guiPosition(Vector2 pos, Angle orientation);
