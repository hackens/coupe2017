#pragma once

#include <string>
#include <vector>
#include "vector.h"
#include "graph.h"
#include "motion.h"

class PrimTask;

namespace Behavior{

  class Task{
    public:
      explicit Task(std::string name);
      virtual ~Task(){}

      /// do one step of the task, the boolean at true means the task is not finished
      virtual bool step() =0;

      /// return true when task has failed
      virtual bool failed() = 0;

      void execute(){
        while(step());
      }

      bool done() {return mDone;}
    protected:
      std::string mName;
      bool mDone;
  };

  class DirectGoTo : public Task{
      Vector2 mDest;
      Distance mEpsilon;
      Angle mAlpha;
    public:
      /// go directly to dest with precision epsilon in Distance and Alpha in angle.
      DirectGoTo(std::string name,Vector2 dest,Distance epsilon = 0.1_cm,Angle alpha = 0.03);
      void rearm(Vector2 dest);
      bool step();
      bool failed(){return false;}

  };

  class DirectGoToG : public Task{
      Vector2 mDest;
      Distance mEpsilon;
      Angle mAlpha;
    public:
      /// go directly to dest with precision epsilon in Distance and Alpha in angle.
      DirectGoToG(std::string name,Vector2 dest,Distance epsilon = 0.1_cm,Angle alpha = 0.03);
      void rearm(Vector2 dest);
      bool step();
      bool failed(){return false;}

  };


  class Back : public Task{
      Distance mDist;
    public:
      Back(const std::string & name, Distance d) : Task(name), mDist(d){}
      bool step(){
        Mobility::Motion m(-mDist,0);
        m.execute();
        return false;
      }
      bool failed(){return false;}
  };

  /// Grabs a cylinder from a current position
  class GrabFromHere : public Task{
      Vector2 mDest;
      DirectGoTo mDgt;
      int mTryNum = 0;
      bool mFailed;
      bool mReleased;
      bool mRocket;
      bool mDepDone;
      int mNumCyl;
      // Must be at this distance to grab.
      static const Distance minDist;
      static const Distance apprSteps;
      static Vector2 getVec(Vector2& vec);
    public:
      GrabFromHere(const std::string& name, Vector2 cylPos,
                   bool rocket = false, int numCyl = 1);
      bool step();
      bool failed(){return mFailed;}
  };

  class GotoGraph : public Task{
      int currentDest;
      int finalDest;
      Graph& mGraph;
    public :
      GotoGraph(const std::string& name, Graph& g, int startVert, int destVert);
      bool step();
      bool failed(){return false;}
  };

  /// ReleaseDir from a given point in a given direction
  class ReleaseDir : public Task{
      DirectGoTo mDgt;
      Angle mOrientation;
      bool mAll;
    public:
      ReleaseDir(const std::string& name, Vector2 startPos,Angle startOri, bool all = false);
      bool step();
      bool failed(){return false;}
  };

  class SimpleGrab : public Task{
      
  };

};
