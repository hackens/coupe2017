#include "vector.h"
#include <cmath>
#include <iostream>

Vector2::Vector2() {
}

Vector2::Vector2(Distance x, Distance y) :
  mX(x), mY(y) {
}

Vector2::Vector2(Distance r, Angle theta)
  : mX(r * cos(theta)),mY(r*sin(theta)){
}


Vector2& Vector2::operator+= (const Vector2& oth) {
  mX += oth.mX;
  mY += oth.mY;
  return *this;
}

Vector2& Vector2::operator-= (const Vector2& oth) {
  mX -= oth.mX;
  mY -= oth.mY;
  return *this;
}

Vector2& Vector2::operator*= (double scal) {
  mX *= scal;
  mY *= scal;
  return *this;
}

Distance Vector2::x() const {
  return mX;
}

Distance Vector2::y() const {
  return mY;
}

Distance Vector2::Norm()const {
  return Distance (sqrt(mX.meters() * mX.meters() + mY.meters()*mY.meters()));
}
void Vector2::reNorm(Distance norm){
  double scal = norm / Norm();
  (*this)*= scal;
}

Angle Vector2::orientation() const {
  return atan2(mY.meters(),mX.meters());
}

Vector2 operator+(Vector2 v1, const Vector2& v2){
  v1 += v2;
  return v1;
}

Vector2 operator-(Vector2 v1, const Vector2& v2){
  v1 -= v2;
  return v1;
}

Vector2 operator*(Vector2 v, const double s){
  v *= s;
  return v;
}


std::ostream& operator<<(std::ostream& out, Vector2 vec){
  out << "(" << vec.x() << "," << vec.y() << ")";
  return out;
}
