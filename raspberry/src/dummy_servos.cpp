#include "dummy_servos.h"

using namespace Com;

std::string DummyServos::readLine(bool wait) {
  if (mState==0) {
    if (wait) {
      std::cerr<<"Warning : infinite loop in dummy_servos !"<<std::endl;
      while (true);
    }
    else
      return std::string();
  }
  else if (mState==1) {
    mState=2;
    return std::string("Doing whatever a dummy does...");
  }
  else if (mState==2) {
    mState=0;
    return std::string("done");
  }
  return "";
}

std::string DummyServos::readLine(int) {
  return readLine(false);
}

void DummyServos::flush() {
  mState=0;
}

void DummyServos::writeMessage(std::string) {
  mState=1;
}


