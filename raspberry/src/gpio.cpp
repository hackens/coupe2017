#include "gpio.h"

#include <fstream>
#include <iostream>
#include <sstream>

int Gpio::IN = 0;
int Gpio::OUT = 1;

Gpio::Gpio(const int g) : mPin(g), mDirection(-1) {
  std::stringstream pinNum;
  pinNum<<mPin<<"\n";
  writeVal("/sys/class/gpio/export",pinNum.str());
}

Gpio::Gpio(const int g, const int d) : mPin(g), mDirection(d) {
  std::stringstream pinNum;
  pinNum<<mPin<<"\n";
  writeVal("/sys/class/gpio/export",pinNum.str());
  setDirection(d);
  setValue(0);
}

int Gpio::pin() const {
  return mPin;
}

void Gpio::setDirection(const int d) {
  mDirection=d;
  std::stringstream pinPath;
  pinPath<<"/sys/class/gpio/gpio"<<mPin;
  writeVal(std::string(pinPath.str())+std::string("/direction"),mDirection==OUT ? "out\n" : "in\n");
}

void Gpio::setValue(const int v) {
  std::stringstream pinPath;
  pinPath<<"/sys/class/gpio/gpio"<<mPin;
  writeVal(std::string(pinPath.str())+std::string("/value"),v?"1\n":"0\n");
}

void Gpio::writeVal(std::string f, std::string val) {
  std::cout<<"Write "<<val.substr(0,val.size()-1)<<" to "<<f<<std::endl;
  std::ofstream s(f);
  s<<val;
  s.close();
}

