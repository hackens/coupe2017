#include <unistd.h>
#include "gui_interface.h"
#include "motion.h"
#include "robot.h"
#include "sstream"
#include "distance.h"
#include "message.h"
#include "logging.h"

using namespace Logging;

namespace Mobility {

  bool Motion::ignoreRadar = false;
  bool Motion::radarState = true;
  double Motion::linearFactor = 6.91;
  double Motion::rotatFactor = 6.89;

  Motion::Motion() {
  }

  Motion::Motion(Distance d, Angle dt, bool slow) : mD(d), mDt(dt), mSlow(slow){
  }

  Distance Motion::d() const noexcept {
    return mD;
  }

  Angle Motion::dt() const noexcept {
    return mDt;
  }
  
  bool Motion::waitOnRadar() {
    std::stringstream ss;
    ss<<"RADAR ";
    Com::Message m(ss.str());
    m.send(Robot::sStepper);
    info<<"Sent \"RADAR\"\n";
    std::string result = Robot::sStepper.readMessage(true).get();
    if (result=="1") {
      error<<"Waiting on radar before starting motion";
      return false;
    }
    return true;
  }

  void Motion::execute() {
    time_t t;
    time(&t);
    if (Robot::sStart!=0 && difftime(Robot::sStart,t)>Robot::sTime)
      return;

    //radarState is true if radar is enabled
    //if(ignoreRadar == radarState){
      if(ignoreRadar){
        Com::Message m("IGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = false;
      }
      else{
        Com::Message m("UNIGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = true;
      }
      //}

    if(mDt == 0){
      if(mSlow){
        Com::Message m("SLOW");
        m.send(Robot::sStepper);
        info<<"Sent \"SLOW\"\n";
        info<<Robot::sStepper.readMessage(true)<<"\n";
      }
      else{
        Com::Message m("FAST");
        m.send(Robot::sStepper);
        info<<"Sent \"FAST\"\n";
        info<<Robot::sStepper.readMessage(true)<<"\n";
      }

      if(mD.pos()) {
        Robot::sDirLeft.setValue(1);
        Robot::sDirRight.setValue(1);

        if (radarState)
          while (!waitOnRadar());

        adv(mD.millimeters()*linearFactor);
      }
      if(mD.neg()) {

        Com::Message m("IGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = false;

        Robot::sDirLeft.setValue(0);
        Robot::sDirRight.setValue(0);
        adv(-mD.millimeters()*linearFactor);

        Com::Message m2("UNIGNORE_RADAR");
        m2.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = true;
      }
      Robot::sPosition += Vector2(mD,Robot::sOrientation);
      info << "Moved of " << Distance (int(mD.millimeters()
                                           *linearFactor)/linearFactor /1000)
           << "in dir" << Robot::sOrientation <<"\n";
    }
    if(mD.zero()){
      Com::Message m("FAST");
      m.send(Robot::sStepper);
      info<<"Sent\n";
      info<<Robot::sStepper.readMessage(true)<<"\n";

      Com::Message m2("IGNORE_RADAR");
      m2.send(Robot::sStepper);
      info<<Robot::sStepper.readMessage(true)<<"\n";
      radarState = false;


      Distance RobotRadius = 10.0_cm;
      std::cout << "Motion turn, Color : " << (Robot::sYellow ? "Yellow" : "Blue") << "\n";
      if(mDt > 0){
        if (Robot::sYellow) {
          Robot::sDirLeft.setValue(1);
          Robot::sDirRight.setValue(0);
        }
        else {
          Robot::sDirLeft.setValue(0);
          Robot::sDirRight.setValue(1);
        }
        adv((RobotRadius * mDt).millimeters()*rotatFactor);
      }
      if(mDt < 0){
        if (Robot::sYellow) {
          Robot::sDirLeft.setValue(0);
          Robot::sDirRight.setValue(1);
        }
        else {
          Robot::sDirLeft.setValue(1);
          Robot::sDirRight.setValue(0);
        }
        adv((RobotRadius * -mDt).millimeters()*rotatFactor);
      }
      Robot::sOrientation += mDt; // TODO make the real movement

      Com::Message m3("UNIGNORE_RADAR");
      m3.send(Robot::sStepper);
      info<<Robot::sStepper.readMessage(true)<<"\n";
      radarState = true;

    }
    guiPosition(Robot::sPosition, Robot::sOrientation);
  }

  void Motion::async() {
    time_t t;
    time(&t);
    if (Robot::sStart!=0 && difftime(Robot::sStart,t)>Robot::sTime)
      return;

    if(ignoreRadar == radarState){
      if(ignoreRadar){
        Com::Message m("IGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = false;
      }
      else{
        Com::Message m("UNIGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = true;
      }
    }

    if(mDt == 0){
      if(mSlow){
        Com::Message m("SLOW");
        m.send(Robot::sStepper);
        info<<"Sent \"SLOW\"\n";
        info<<Robot::sStepper.readMessage(true)<<"\n";
      }
      else{
        Com::Message m("FAST");
        m.send(Robot::sStepper);
        info<<"Sent \"FAST\"\n";
        info<<Robot::sStepper.readMessage(true)<<"\n";
      }

      if(mD.pos()) {
        Robot::sDirLeft.setValue(1);
        Robot::sDirRight.setValue(1);

        while (!waitOnRadar());
        mGoal = mD.millimeters()*linearFactor;
        advAsync();
      }
      if(mD.neg()) {

        Com::Message m("IGNORE_RADAR");
        m.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = false;

        Robot::sDirLeft.setValue(0);
        Robot::sDirRight.setValue(0);
        mGoal = -mD.millimeters()*linearFactor;
        advAsync();

        Com::Message m2("UNIGNORE_RADAR");
        m2.send(Robot::sStepper);
        info<<Robot::sStepper.readMessage(true)<<"\n";
        radarState = true;
      }
      //Robot::sPosition += Vector2(mD,Robot::sOrientation);
      info << "Try to move async of " << Distance (int(mD.millimeters()
                                           *linearFactor)/linearFactor /1000)
           << "in dir" << Robot::sOrientation <<"\n";
    }
    if(mD.zero()){
      Com::Message m("SLOW");
      m.send(Robot::sStepper);
      info<<"Sent\n";
      info<<Robot::sStepper.readMessage(true)<<"\n";

      Com::Message m2("IGNORE_RADAR");
      m2.send(Robot::sStepper);
      info<<Robot::sStepper.readMessage(true)<<"\n";
      radarState = false;


      Distance RobotRadius = 10.0_cm;
      std::cout << "Motion turn, Color : " << (Robot::sYellow ? "Yellow" : "Blue") << "\n";
      if(mDt > 0){
        if (Robot::sYellow) {
          Robot::sDirLeft.setValue(1);
          Robot::sDirRight.setValue(0);
        }
        else {
          Robot::sDirLeft.setValue(0);
          Robot::sDirRight.setValue(1);
        }
        mGoal = (RobotRadius * mDt).millimeters()*rotatFactor;
        advAsync();
      }
      if(mDt < 0){
        if (Robot::sYellow) {
          Robot::sDirLeft.setValue(0);
          Robot::sDirRight.setValue(1);
        }
        else {
          Robot::sDirLeft.setValue(1);
          Robot::sDirRight.setValue(0);
        }
        mGoal = (RobotRadius * -mDt).millimeters()*rotatFactor;
        advAsync();
      }
      //Robot::sOrientation += mDt; // TODO make the real movement

      Com::Message m3("UNIGNORE_RADAR");
      m3.send(Robot::sStepper);
      info<<Robot::sStepper.readMessage(true)<<"\n";
      radarState = true;

    }
    guiPosition(Robot::sPosition, Robot::sOrientation);
  }

  bool Motion::finished(){
    info<<"test finished\n";
    std::stringstream ss;
    ss<<"DONE?  ";
    Com::Message m(ss.str());
    m.send(Robot::sStepper);
    info<<"Sent\n";
    info<<Robot::sStepper.readMessage(true)<<"\n";
    std::string result = Robot::sStepper.readMessage(true).get();
    info<<result<<"\n";
    return result.substr(0,4) == "done";
  }

  void Motion::update(){
    if(finished()){
      if(mGoal == 0) return;
      else{
        if(mDt == 0){
        Robot::sPosition += Vector2(mD.sign() * 5000 / 1000.0 / linearFactor,Robot::sOrientation);
        info << "update async of " << Distance (int(mD.millimeters()
                                                    *linearFactor)/linearFactor /1000)
             << "in dir" << Robot::sOrientation <<"\n";
        }
        if(mD.zero()){
          Distance RobotRadius = 10.0_cm;
          std::cout << "Motion turn update, Color : "
                    << (Robot::sYellow ? "Yellow" : "Blue") << "\n";
          if(mDt > 0){
            mGoal = (RobotRadius * mDt).millimeters()*rotatFactor;
            Robot::sOrientation += 5000/ rotatFactor / 1000.0 / RobotRadius.meters();
          }
          if(mDt < 0){
            Robot::sOrientation += 5000/ rotatFactor / 1000.0 / RobotRadius.meters();
          }
        }
        advAsync();
      }
    }
    else return;
  }

  void Motion::end(){
    if(finished()){
      
    }
  }
  void Motion::adv(int mm) {
    guiPosition(Robot::sPosition, Robot::sOrientation);
    while(mm > 800){
      adv(800);
      mm -= 800;
    }
    info<<"adv("<<mm<<")\n";
    std::stringstream ss;
    ss<<"FWD  "<<mm;
    Com::Message m(ss.str());
    m.send(Robot::sStepper);
    info<<"Sent\n";
    info<<Robot::sStepper.readMessage(true)<<"\n";
    std::string result = Robot::sStepper.readMessage(true).get();
    info<<result<<"\n";
    usleep(50000);
    if (result.substr(0,5)=="error") {
      //TODO retry limit
      usleep(500000);
      return adv(mm);
    }
    guiPosition(Robot::sPosition, Robot::sOrientation);
  }

  void Motion::advAsync() {
    guiPosition(Robot::sPosition, Robot::sOrientation);
    int todo = mGoal > 5000 ? 5000 : mGoal;
    info<<"advAsync("<<todo<<")\n";
    std::stringstream ss;
    ss<<"ASYNC  " << todo;
    Com::Message m(ss.str());
    m.send(Robot::sStepper);
    info<<"Send \"async\"\n";
    info<<Robot::sStepper.readMessage(true)<<"\n";
    std::string result = Robot::sStepper.readMessage(true).get();
    info<<result<<"\n";
    usleep(50000);
    if (result.substr(0,5)=="error") {
      //TODO retry limit
      usleep(500000);
      return advAsync();
    }
    mGoal -= todo;
    guiPosition(Robot::sPosition, Robot::sOrientation);
  }

};

void async(){
  
}
