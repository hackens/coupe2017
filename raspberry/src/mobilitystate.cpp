#include "mobilitystate.h"

#include "motion.h"
#include <math.h>

#include <assert.h>

namespace Mobility {
  MobilityState::MobilityState() {
  }

  MobilityState::MobilityState(Distance x, Distance y, Angle h) :
    mX(x), mY(y), mH(h) {
  }

  Distance MobilityState::x() const noexcept {
    return mX;
  }

  Distance MobilityState::y() const noexcept {
    return mY;
  }

  Angle MobilityState::h() const noexcept {
    return mH;
  }
  /// \todo finish the implementation
  const MobilityState& MobilityState::operator+=(const Motion& m) {
    assert(m.d().zero() || m.dt()==0);
    mX += m.d() * sin(mH);
    mY += m.d() * cos(mH);
    mH += m.dt();
    return *this;
  }
};

