#include "message.h"
#include <cstdarg>
#include <sstream>
#include "com.h"

using namespace Com;

Message::Message(std::string verb, std::vector<std::string> args) {
  mSentence = verb;
  for (const auto& a : args) {
    mSentence += " " + a;
  }
}

Message::Message(std::string sentence) : 
  mSentence(sentence) {
}

void Message::send(const Serial& s) const {
  std::stringstream ss;
  ss<<mSentence<<"\n";
  s.writeMessage(ss.str());
}

std::string Message::get() const {
  return mSentence;
}

std::ostream& Com::operator<<(std::ostream& o, const Message& m) {
  o<<"\""<<m.get()<<"\"";
  return o;
}

