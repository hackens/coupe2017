#ifndef GRAPH_H
#define GRAPH_H

#include "vector.h"
#include <set>
#include <vector>
#include <map>

class Graph{
  public :
    class V;
  private:
    struct intV{ //vertex
        friend V;
        Vector2 mPos;
        std::set<int> mNeigh;
        std::string mName;
        bool active;
    };
    std::vector<intV> mPts;
    std::map<std::string,int> mName2V;
  public:
    class V{
        friend Graph;
        Graph& g;
        int mNum;
        intV& th(){return g.mPts[mNum];}
        const intV& th() const {return g.mPts[mNum];}
        V(Graph& graph, int num) : g(graph), mNum(num){}
      public :
        V& operator-(V& oth); // bond
        std::string name() const {return th().mName;}
        void setName(std::string name);
        const std::set<int> neigh() const {return th().mNeigh;}
        Distance dist(const V& oth) const{
          return (th().mPos - oth.th().mPos).Norm();
        }
        Vector2 pos() const {return th().mPos;}
        int num() const {return mNum;}
        bool& active(){return th().active;}

    };
    Graph();
    V operator+=(Vector2 v); // add vertex;
    V operator[](int i){return V(*this,i);}
    V operator[](const std::string& s){return V(*this,mName2V.at(s));}
    size_t size() const {return mPts.size();}
    bool in(const std::string& s){return mName2V.count(s);}
};

inline std::ostream& operator<<(std::ostream& out, const Graph::V v){
  return out << v.num() << " i.e " << v.name();
}

void getMap(Graph& g);

#endif
