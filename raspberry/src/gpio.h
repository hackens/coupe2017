#pragma once

#include <string>

class Gpio {
  public:
    Gpio(const int gpionum);
    Gpio(const int gpionum, const int direction);
    int pin() const;

    void setDirection(const int direction);
    void setValue(const int value);

    static int OUT;
    static int IN;

  private:
    void writeVal(std::string f, std::string val);

    const int mPin;
    int mDirection;
};

