#pragma once
#include "com.h"
#include <string>

namespace Com {
  class DummySteppers : public DummySerial {
    public:
      virtual std::string readLine(bool wait);
      virtual std::string readLine(int waitUS);
      virtual void flush();
      virtual void writeMessage(std::string s);

    private:
      int mState=0;
  };
};
