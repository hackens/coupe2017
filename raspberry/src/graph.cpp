#include "graph.h"
#include <cassert>
#include <iostream>

using namespace std;

Graph::V& Graph::V::operator-(Graph::V& other){
  other.th().mNeigh.insert(mNum);
  th().mNeigh.insert(other.mNum);
  return other;
}
void Graph::V::setName(std::string nName){
  assert(!g->mName2V.count(name));
  th().mName = nName;
  g.mName2V[nName] = mNum;
}

Graph::V Graph::operator+=(Vector2 v){
  mPts.push_back(intV());
  mPts.back().active = true;
  mPts.back().mPos = v;
  return V(*this,mPts.size() -1);
}

Graph::Graph(){
  getMap(*this);
}

void getMap(Graph& g){ // fill Graph.
  assert(g.size() == 0);
  auto startBig = g += Vector2(18_cm,18_cm);
  auto startSmall = g += Vector2(18_cm,89_cm);
  auto frontTBlue = g += Vector2(25_cm,115_cm);
  auto nearBMulti = g += Vector2(60_cm,100_cm);
  auto nearBMultiEast = g += Vector2(60_cm,120_cm);
  auto nearBMultiSouth = g += Vector2(80_cm,100_cm);
  auto nearBMultiSE = g += Vector2(74_cm,114_cm);
  auto nearWPool = g += Vector2(54_cm,65_cm);
  auto nearWPoolSouth = g += Vector2(85_cm,65_cm);
  auto westBCyl = g += Vector2(60_cm,20_cm);
  auto nearWestGoal = g += Vector2(92.5_cm,25_cm);
  auto centerWMulti = g += Vector2(110_cm,50_cm);
  auto armWMulti = g += Vector2(140_cm,90_cm);
  auto armWNorth = g += Vector2(110_cm,90_cm);
  auto armWNW = g += Vector2(129_cm,79_cm);
  auto armWWest = g += Vector2(140_cm,70_cm);
  auto southBCyl = g += Vector2(185_cm,80_cm);
  auto southBCylNorth = g += Vector2(170_cm,80_cm);
  auto swsmallPool = g += Vector2(187_cm,115_cm);
  auto swPoolEntry = g += Vector2(150_cm,50_cm);
  auto westYCylEast = g += Vector2(130_cm,30_cm);


  startBig.setName("startBig");
  startSmall.setName("startSmall");
  frontTBlue.setName("frontTBlue");
  nearBMulti.setName("nearBMulti");
  nearBMultiEast.setName("nearBMultiEast");
  nearBMultiSouth.setName("nearBMultiSouth");
  nearBMultiSE.setName("nearBMultiSE");
  nearWPool.setName("nearWPool");
  nearWPoolSouth.setName("nearWPoolSouth");
  westBCyl.setName("westBCyl");
  nearWestGoal.setName("nearWestGoal");
  centerWMulti.setName("centerWMulti");
  armWMulti.setName("armWMulti");
  armWNorth.setName("armWNorth");
  armWNW.setName("armWNW");
  armWWest.setName("armWWest");
  southBCyl.setName("southBCyl");
  southBCylNorth.setName("southBCylNorth");
  swsmallPool.setName("swsmallPool");
  swPoolEntry.setName("swPoolEntry");
  westYCylEast.setName("westYCylEast");


  nearBMulti.active() = false;
  nearWPool.active() = false;
  westBCyl.active() = false;
  centerWMulti.active() = false;
  armWMulti.active() = false;
  southBCyl.active() = false;
  swsmallPool.active() = false;


  auto armCenter = g += Vector2(100_cm,150_cm);
  armCenter.setName("armCenter");

  auto startYSmall = g += Vector2(18_cm, 3_m - 89_cm);
  auto frontTYellow = g += Vector2(22_cm, 3_m - 115_cm);
  auto nearYMulti = g += Vector2(60_cm, 3_m - 100_cm);
  auto nearYMultiWest = g += Vector2(60_cm, 3_m - 115_cm);
  auto nearYMultiSouth = g += Vector2(75_cm, 3_m - 100_cm);
  auto nearYMultiSW = g += Vector2(71_cm, 3_m - 111_cm);
  auto nearEPool = g += Vector2(54_cm, 3_m - 65_cm);
  auto nearEPoolSouth = g += Vector2(80_cm, 3_m - 65_cm);
  auto eastYCyl = g += Vector2(60_cm, 3_m - 20_cm);
  auto nearEastGoal = g += Vector2(92.5_cm, 3_m - 25_cm);
  auto centerEMulti = g += Vector2(110_cm, 3_m - 50_cm);
  auto armEMulti = g += Vector2(140_cm, 3_m - 90_cm);
  auto armENorth = g += Vector2(110_cm, 3_m - 90_cm);
  auto armENE = g += Vector2(129_cm, 3_m - 79_cm);
  auto armEEast = g += Vector2(140_cm, 3_m - 70_cm);
  auto southYCyl = g += Vector2(185_cm, 3_m - 80_cm);
  auto southYCylNorth = g += Vector2(170_cm, 3_m - 80_cm);
  auto sesmallPool = g += Vector2(187_cm, 3_m - 115_cm);
  auto sePoolEntry = g += Vector2(150_cm, 3_m - 50_cm);
  auto eastBCylWest = g += Vector2(130_cm, 3_m - 15_cm);

  startYSmall.setName("startYSmall");
  frontTYellow.setName("frontTYellow");
  nearYMulti.setName("nearYMulti");
  nearYMultiWest.setName("nearYMultiWest");
  nearYMultiSouth.setName("nearYMultiSouth");
  nearYMultiSW.setName("nearYMultiSW");
  nearEPool.setName("nearEPool");
  nearEPoolSouth.setName("nearEPoolSouth");
  eastYCyl.setName("eastYCyl");
  nearEastGoal.setName("nearEastGoal");
  centerEMulti.setName("centerEMulti");
  armEMulti.setName("armEMulti");
  armENorth.setName("armENorth");
  armENE.setName("armENE");
  armEEast.setName("armEEast");
  southYCyl.setName("southYCyl");
  southYCylNorth.setName("southYCylNorth");
  sesmallPool.setName("sesmallPool");
  sePoolEntry.setName("sePoolEntry");
  eastBCylWest.setName("eastBCylWest");



  // west edges
  startBig - startSmall - frontTBlue;
  startSmall - nearBMulti;
  startSmall - nearBMultiEast;
  frontTBlue - nearBMulti;
  frontTBlue - nearBMultiEast;
  nearBMulti - nearBMultiEast;
  nearBMulti - nearBMultiSouth;
  nearBMulti - nearBMultiSE;
  nearBMultiEast - nearBMultiSE - nearBMultiSouth;
  nearBMulti - nearWPool - westBCyl;
  nearBMulti - nearWPoolSouth - westBCyl;
  nearWPool - nearWPoolSouth;
  nearBMultiSouth - nearWPool;
  nearBMultiSouth - nearWPoolSouth;
  westBCyl - nearWestGoal - nearWPoolSouth;
  nearWestGoal - centerWMulti - nearWPoolSouth;
  centerWMulti - armWNorth - nearBMultiSouth;
  armWNorth - nearWPoolSouth;
  armWNorth - nearBMulti;

  armWMulti - armWNorth;
  armWMulti - armWWest;
  armWMulti - armWNW;
  armWNorth - armWNW - armWWest;

  swsmallPool - southBCyl - southBCylNorth - swsmallPool;
  southBCylNorth - armWWest - swPoolEntry - southBCylNorth;
  southBCyl - swPoolEntry;
  swPoolEntry - armWNorth;
  swPoolEntry - armWNW;
  swPoolEntry - armWWest;
  swPoolEntry - westYCylEast - nearWestGoal;
  westYCylEast - centerWMulti;

  //center edges
  frontTBlue - frontTYellow;
  frontTBlue - nearYMultiWest;
  nearBMultiEast - nearYMultiWest;
  nearBMultiEast - frontTYellow;

  armCenter - armWNorth;
  armCenter - nearBMultiEast;
  armCenter - nearBMultiSE;
  armCenter - nearBMultiSouth;
  armCenter - armENorth;
  armCenter - nearYMultiWest;
  armCenter - nearYMultiSW;
  armCenter - nearYMultiSouth;


  // east edges
  startYSmall - frontTYellow;
  startYSmall - nearYMulti;
  startYSmall - nearYMultiWest;
  frontTYellow - nearYMulti;
  frontTYellow - nearYMultiWest;
  nearYMulti - nearYMultiWest;
  nearYMulti - nearYMultiSouth;
  nearYMulti - nearYMultiSW;
  nearYMultiWest - nearYMultiSW - nearYMultiSouth;
  nearYMulti - nearEPool - eastYCyl;
  nearYMulti - nearEPoolSouth - eastYCyl;
  nearEPool - nearEPoolSouth;
  nearYMultiSouth - nearEPool;
  nearYMultiSouth - nearEPoolSouth;
  eastYCyl - nearEastGoal - nearEPoolSouth;
  nearEastGoal - centerEMulti - nearEPoolSouth;
  centerEMulti - armENorth - nearYMultiSouth;
  armENorth - nearEPoolSouth;
  armENorth - nearYMulti;

  armEMulti - armENorth;
  armEMulti - armEEast;
  armEMulti - armENE;
  armENorth - armENE - armEEast;

  sesmallPool - southYCyl - southYCylNorth - sesmallPool;
  southYCylNorth - armEEast - sePoolEntry - southYCylNorth;
  southYCyl - sePoolEntry;
  sePoolEntry - armENorth;
  sePoolEntry - armENE;
  sePoolEntry - armEEast;
  sePoolEntry - eastBCylWest - nearEastGoal;
  eastBCylWest - centerEMulti;

  cout << " name : " << startBig <<endl;

}

