#include "delimitedstream.h"
#include <iostream>
#include <cstdint>

void writeDelimited(std::ostream& out, const std::string& data) {
  uint32_t size = data.size();
  out.write(reinterpret_cast<const char*>(&size),4);
  out.write(data.c_str(),data.size());
  std::cout<<"[DELIM] Wrote "<<size<<" bytes"<<std::endl;
}

DelimitedIStream::DelimitedIStream(std::ifstream* is) :
  mStream(is), mBuffer() {
}

DelimitedIStream::DelimitedIStream() :
  mStream(new std::ifstream()), mBuffer() {
  std::cout<<"Constructed delimiteistream good="<<mStream->good()<<std::endl;
}

DelimitedIStream:: ~DelimitedIStream() {
  delete mStream;
  mStream=nullptr;
}

std::ifstream* DelimitedIStream::get() {
  return mStream;
}

std::string DelimitedIStream::readDelimited() {
  if (!*mStream) {
    std::cerr<<"[DELIM] invalid stream good="<<mStream->good()<<" eof="<<mStream->eof()<<" fail="<<mStream->fail()<<" bad="<<mStream->bad()<<" rdstate="<<mStream->rdstate()<<std::endl;
    exit(1);
  }

  char buffer[100];
  //std::cerr<<"[DELIM] stream good="<<mStream->good()<<" eof="<<mStream->eof()<<" fail="<<mStream->fail()<<" bad="<<mStream->bad()<<" rdstate="<<mStream->rdstate()<<std::endl;
  while (true) {
    int read = mStream->readsome(buffer,100);
    if (read==0)
      break;

    mBuffer += std::string(buffer,read);
    std::cout<<"[DELIM] pushed "<<read<<" chars in buffer"<<std::endl;
  }
  if (mBuffer.size()<4) {
    return std::string();
  }
  else {
    uint32_t size = *(reinterpret_cast<const uint32_t*>(mBuffer.c_str()));
    std::cout<<"[DELIM] trying to read "<<size<<" bytes but only "<<mBuffer.size()<<" present"<<std::endl;
    if (mBuffer.size()<4+size) {
      return std::string();
    }
    else {
      std::string ret = mBuffer.substr(4,size);
      mBuffer = mBuffer.substr(4+size);
      return ret;
    }
  }
}

