#pragma once

#include "angle.h"
#include "distance.h"
#include <iostream>

class Vector2 {
  public:
    Vector2();
    Vector2(Distance x, Distance y);
    Vector2(Distance r, Angle theta);

    Vector2& operator+= (const Vector2& oth);
    Vector2& operator-= (const Vector2& oth);
    Vector2& operator*= (double scal);

    Distance x() const;
    Distance y() const;

    Distance Norm() const;
    void reNorm(Distance norm);

    Angle orientation() const ;

  protected:
    Distance mX = 0.0f;
    Distance mY = 0.0f;
};

Vector2 operator+(Vector2 v1, const Vector2& v2);
Vector2 operator-(Vector2 v1, const Vector2& v2);
Vector2 operator*(Vector2 v, const double s);

std::ostream& operator<<(std::ostream& out, Vector2 vec);

