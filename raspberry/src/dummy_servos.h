#pragma once
#include "com.h"
#include <string>

namespace Com {
  class DummyServos : public DummySerial {
    public:
      virtual ~DummyServos() {};
      virtual std::string readLine(bool wait);
      virtual std::string readLine(int waitUS);
      virtual void flush();
      virtual void writeMessage(std::string s);

    private:
      int mState=0;
  };
};
