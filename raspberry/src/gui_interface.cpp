#include "gui_interface.h"
#include "delimitedstream.h"

std::shared_ptr<std::ofstream> gui_fifo;

void guiPosition(Vector2 pos, Angle orientation) {
  Position p;
  p.set_x(pos.x().meters());
  p.set_y(pos.y().meters());
  p.set_heading(orientation);
  if(gui_fifo==nullptr)
    return;
  std::string str;
  p.SerializeToString(&str);
  writeDelimited(*gui_fifo,str);
  gui_fifo->flush();
}

