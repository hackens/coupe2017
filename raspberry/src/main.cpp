#include <iostream>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sstream>
#include <deque>

#include "gui_interface.h"
#include "com.h"
#include "logging.h"
#include "mobilitystate.h"
#include "motion.h"
#include "distance.h"
#include "message.h"
#include "gpio.h"
#include "actions.h"
#include "task.h"
#include "robot.h"
#include "task.h"
#include "command.h"

using namespace std;
using namespace Logging;
using namespace Mobility;
using namespace Com;
using namespace Behavior;

Serial* serial;
Gpio* dirLeft;
Gpio* dirRight;

/*int fullturn=4320;
int meter = 7072;


int compMM(int mm) {
  return int(float(mm)*float(meter)/1000.0f);
}

int compDeg(int deg) {
  return int(float(deg)*float(fullturn)/360.0f);
  }*/

int main(int argc, char ** argv) {
  try {
    if(argc > 1){
      if (std::string(argv[1])=="blue") {
        std::cout<<"COLOR BLUE"<<std::endl;
        Robot::sYellow=false;
      }
      else if (std::string(argv[1])=="yellow") {
        std::cout<<"COLOR YELLOW"<<std::endl;
        Robot::sYellow=true;
      }
      else {
        cmd(argc,argv);
        return 0;
      }
    }
    
    struct stat st;
    if (!stat("gui_fifo",&st) && S_ISFIFO(st.st_mode)) {
      std::cout<<"Connecting to gui_fifo..."<<std::endl; 
      gui_fifo = make_shared<std::ofstream>("gui_fifo",std::ofstream::out);
      std::cout<<"fifo is "<<gui_fifo.get()<<std::endl;
    }
    else {
      std::cerr<<"Could not find GUI fifo"<<std::endl;
      }

    //Arduino boot sequence
    {
      int time=0;
      while (time<5000) {
        usleep(200000); time+=200;
        Robot::sRunLed.setValue(0);
        usleep(200000); time+=200;
        Robot::sRunLed.setValue(1);
      }
    }

    {
      Robot::sStepper.writeMessage("RESET\n");
      usleep(200000);
      Robot::sStepper.flush();
    }

    Robot::sAction.detachAll();
    Robot::sAction.lift();
    Robot::sAction.lower();
    Robot::sAction.lock();
    Robot::sAction.unlock();
    Robot::sAction.release();
    Mobility::Motion m(1_cm,0);
    m.execute();
    Mobility::Motion m2(-1_cm,0);
    m2.execute();

    Robot::sStepper.flush();
    Robot::sServos.flush();
    // match petit
    /*deque<Task*> TODOlist = {
      new DirectGoTo("goto",Vector2(55_cm,115_cm)),
      new GrabFromHere("gfh",Vector2(4_cm,115_cm)), // TODO tweak this value for time
      new Back("back", 10_cm),
      new DirectGoTo("return", Vector2(20_cm,100_cm)),
      new ReleaseDir("rd", Vector2(20_cm,100_cm),-3*M_PI/4),
      new Back("back", 5_cm),
      new DirectGoTo("goto",Vector2(25_cm,115_cm)),
      new GrabFromHere("gfh",Vector2(4_cm,115_cm)), // TODO tweak this value for time
      new Back("back", 10_cm),
      new DirectGoTo("return", Vector2(20_cm,100_cm)),
      new ReleaseDir("rd", Vector2(20_cm,100_cm),-3*M_PI/4),
      new Back("back", 5_cm),
      };*/
    // match petit ambitieux
    /*deque<Task*> TODOlist = {
      new DirectGoTo("goto",Vector2(55_cm,115_cm)),
      new GrabFromHere("gfh",Vector2(4_cm,115_cm),true,1), // TODO tweak this value for time
      new Back("back", 10_cm),
      new GotoGraph("gg",Robot::sGraph,Robot::sGraph["frontTBlue"].num()
                    ,Robot::sGraph["armWNorth"].num()),
      new GrabFromHere("gfh 2", Robot::sGraph["armWMulti"].pos()),
      new Back("back 2", 5_cm),
      new DirectGoTo("goto arm", Vector2(135_cm,84_cm)),
      new ReleaseDir("rel all", Vector2(140_cm,88_cm),+ M_PI/4)
      };*/
    /*new GotoGraph("gg",Robot::sGraph,Robot::sGraph["frontTBlue"].num()
      ,Robot::sGraph["armWNorth"].num()),
      new GrabFromHere("gfh 2", Robot::sGraph["armWMulti"].pos()),
      new Back("back 2", 5_cm),
      new DirectGoTo("goto arm", Vector2(135_cm,84_cm)),
      new ReleaseDir("rel all", Vector2(140_cm,88_cm),+ M_PI/4)*/

    //match petit avec gros
    deque<Task*> TODOlist = {
      new DirectGoTo("goto",  Robot::sYellow ? Vector2(49_cm,89_cm) :Vector2(45_cm,115_cm)),
      new DirectGoTo("goto", Robot::sYellow ? Vector2(55_cm,95_cm) :Vector2(55_cm,105_cm)),
      new DirectGoToG("gotog",  Robot::sYellow ? Vector2(80_cm,120_cm) : Vector2(80_cm,80_cm)),
      new DirectGoTo("gotog", Vector2(120_cm,70_cm)),// 120 70
      new ReleaseDir("rd", Vector2(133_cm,83_cm), M_PI/4),// 133 83
      new Back("backf",10_cm),
      new DirectGoTo("goto", Vector2(100_cm,113_cm)),
      new DirectGoTo("goto", Vector2(30_cm,113_cm)),
      new GrabFromHere("gfh",Vector2(4_cm,113_cm)), // TODO tweak this value for time
      new Back("back", 10_cm),
      new DirectGoTo("return", Vector2(20_cm,100_cm)),
      new ReleaseDir("rd", Vector2(20_cm,100_cm),-3*M_PI/4),
      new Back("backf",10_cm)
      /*new DirectGoTo("goto", Vector2(30_cm,115_cm)),
        new GrabFromHere("gfh",Vector2(4_cm,115_cm)), // TODO tweak this value for time
        new Back("back", 10_cm),
        new DirectGoTo("return", Vector2(20_cm,100_cm)),
        new ReleaseDir("rd", Vector2(20_cm,100_cm),-3*M_PI/4)*/
    };


    // homo gros
    /*deque<Task*> TODOlist = {
      new DirectGoTo("goto",Robot::sGraph["nearBMulti"].pos()),
      new DirectGoTo("goto",Robot::sGraph["armWMulti"].pos()),
      };*/


    usleep(500000);

    if (!Robot::sAction.getSensor(Action::START)) {
      info << "Start signal is enabled" << "\n";
      while (!Robot::sAction.getSensor(Action::START)) {
        info<<"Waiting for start signal. Color is "
            << ((Robot::sYellow) ? "Yellow" : "Blue")
            << "\n";
      }
    }
    else {
      info << "Start signal is not enabled" << "\n";
    }

    {
      Robot::sStepper.writeMessage("TIMER 90\n");
      Robot::sServos.writeMessage("TIMER 90\n");
      usleep(200000);
      Robot::sStepper.flush();
      Robot::sServos.flush();
      time(&Robot::sStart);
      Robot::sTime = 90;
    }

    while (!TODOlist.empty() and time(NULL) < Robot::sStart + Robot::sTime -1) {
      bool b = TODOlist.front()->step();
      if (!b) {
        delete TODOlist.front();
        TODOlist.pop_front();
      }
    }

    info << Robot::sPosition.x().meters() << " , " << Robot::sPosition.y().meters() << '\n';

    while(time(NULL) < Robot::sStart + Robot::sTime);
    debug << "time :" << time(NULL) << ", time limitx" << Robot::sStart + Robot::sTime << "\n";

    Robot::sAction.launch_rocket();
    Robot::sAction.detachAll();
  }
  catch (std::string s) {
    error << "[ERROR]" << s << "\n";
    Robot::sRunLed.setValue(0);
    Robot::sErrorLed.setValue(0);
  }

  Robot::sRunLed.setValue(0);
  return 0;
}

