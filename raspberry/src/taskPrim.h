#pragma once

#include <string>
#include "motion.h"

namespace Behavior
{
  class TaskPrim{

  public:
    explicit TaskPrim(std::string name);
    virtual void execute() =0;

  private:
    std::string mName;
  };

  class MotionTask : public TaskPrim{
    Mobility::Motion mMotion;
  public:
    explicit MotionTask(Mobility::Motion motion,std::string name);
    void execute();
  };
};
