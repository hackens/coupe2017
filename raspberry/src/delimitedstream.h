#pragma once
#include <fstream>

void writeDelimited(std::ostream& out, const std::string& data);

std::string readDelimited(std::ifstream& in);

class DelimitedIStream {
  public:
    DelimitedIStream(std::ifstream* is);
    DelimitedIStream();
    ~DelimitedIStream();

    std::ifstream* get();
    std::string readDelimited();

  private:
    std::ifstream* mStream=nullptr;
    std::string mBuffer;
};

