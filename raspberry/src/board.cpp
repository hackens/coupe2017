#include "board.h"

namespace Board {
  Board::Board() {
    //lots of :
    // insert(new MyBoardElement(lots of params))
  }

  BoardElement::BoardElement(std::string name) :
    mName(name) {
  }

  std::string BoardElement::getName() {
    return mName;
  }

  void Board::removeElement(BoardElement* e) {
    if (e->mSymmetric)
      e->mSymmetric->mSymmetric = nullptr;

    mElements.erase(e);
  }

  MonoCylinder::MonoCylinder(Color c) :
    BoardElement(), mColor(c) {
  }

  BiCylinder::BiCylinder(Angle side) :
    BoardElement(), mSide(side) {
  }
};
