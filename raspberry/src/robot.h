#pragma once

#include "com.h"
#include "actions.h"
#include "gpio.h"
#include "vector.h"
#include "graph.h"

/// Contains all static variables
struct Robot{
    static Com::Serial sStepper;
    static Com::Serial sServos;
    static Action sAction;
    static Gpio sDirLeft;
    static Gpio sDirRight;
    static Gpio sRunLed;
    static Gpio sErrorLed;
    static Vector2 sPosition; // estimated value
    static Angle sOrientation;
    static Graph sGraph;
    static bool sYellow;
    static time_t sStart;
    static int sTime;
};
