#pragma once

#include <string>
#include <vector>

/// Contain all class related to communation : arduino/wifi
namespace Com {
  
  class Serial;
  /** \brief Contain a parsable string of a verb and its arguments.

	  \todo implement parsing function.
	  \todo define a protocol of communication with the arduino card.
  */
  class Message {
    public:
      Message(std::string verb, std::vector<std::string> args);
      Message(std::string sentence);
      void send(const Serial& s) const;

      std::string get() const;

    private:
      std::string mSentence;
  };

  std::ostream& operator<<(std::ostream& o, const Message& m);
};

