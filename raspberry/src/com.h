#pragma once

#include <fstream>
#include <termios.h>
#include <iostream>
#include <string>
#include "message.h"

namespace Com {
  class DummySerial {
    public:
      virtual std::string readLine(bool wait) = 0;
      virtual std::string readLine(int waitUS) = 0;
      virtual void flush() = 0;
      virtual void writeMessage(std::string s) = 0;

      virtual ~DummySerial() {}
  };

  /** \brief Represents the communications with a single Serial device.
   * As soon as it is constructed, it must be connected.
   */
  class Serial {
    public:
      /** \brief Connects to a device (for instance /dev/ttyUSB0).
       * 
       * For now the only supported baudrate is 9600. 
       * May throw a string */
      Serial(std::string device, int baudrate, DummySerial* dummy=nullptr);

      /** \brief Disconnects from the serial target. 
       *
       * On some Arduinos, this may
       * reboot them */
      ~Serial();

      /** \brief Reads until a newline. *
       *
       * Returns either a new line (\n character excluded) or an empty 
	   * string if wait=false and a message is not present yet.
       * Will strip all \r characters.
       */
      std::string readLine(bool wait);

      std::string readLine(int waitUS);

      /** \brief Same as readLine, but wraps the string in a message object */
      Message readMessage(bool wait);

      Message readMessage(int waitUS);

      void flush();

      /** \brief Writes a message. Should probably include a newline */
      void writeMessage(std::string s) const;

    private:
      void init(std::string device, int baudrate, DummySerial* dummy=nullptr);

      std::string mDevice;
      int mFd;
      std::string mBuffer;
      mutable DummySerial* mDummy = nullptr;
  };
};
