#pragma once

#include "distance.h"
#include "angle.h"



/// Contain all notions about mouvement and position


namespace Mobility {
  /// Describe a mouvement
  class Motion {
    public:
    /// linear correction factor for execute method
    static double linearFactor;
    /// rotational correction factor for execute method
    static double rotatFactor;
      Motion();
      Motion(Distance d, Angle dt, bool slow = false);

      Distance d() const noexcept;
      Angle dt() const noexcept;
      /// \brief execute the motion
      ///
      /// send command to the Serial object Robot::sStepper
      /// and set Robot::sDirLeft and Robot::sDirRight
      void execute();
      /// execute an non-blocking move
      void async();
      void update(); ///< must be called regularly
      /// check if movement is finished
      bool finished();
      /// End current async move and update position.
      void end();


      static bool ignoreRadar;
    private:
      static bool waitOnRadar();

      static bool radarState;
      void adv(int mm);
      void advAsync(); /// send command depending from mGoal
      Distance mD=0;
      Angle mDt=0;
      bool mSlow;
      int mGoal;
  };
};

