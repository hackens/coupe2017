#include "actions.h"
#include "logging.h"
#include <sstream>

using namespace Com;
using namespace Logging;
using namespace std;

Action::Action(Com::Serial* arduino) :
  mArduino(arduino) {
}
void Action::grab() {
  Message m("GRAB ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::grabSeq() {
  Message m("GRABSEQUENCE ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}
void Action::grabAllSeq(){
  Message m("GRABALLSEQUENCE ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::release() {
  Message m("RELEASE ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::releaseSeq() {
  Message m("RELEASESEQUENCE ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::releaseAllSeq(){
  Message m("RELEASEALLSEQUENCE ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::lock() {
  Message m("LOCK ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::unlock() {
  Message m("UNLOCK ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::lift(){
  Message m("LIFT ");
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::up(){
  Message m("UP ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::lower(){
  Message m("LOWER ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}
void Action::detachAll() {
  for (int i=1; i<=6; i++)
    detachServo(i);
}

void Action::launch_rocket() {
  Message m("LAUNCH_ROCKET");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::lock_rocket() {
  Message m("LOCK_ROCKET");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

void Action::detachServo(int s) {
  std::stringstream ss;
  ss<<"D"<<s<<" ";
  Message m(ss.str());
  m.send(*mArduino);
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
}

bool Action::getSensor(Sensor s){
  stringstream ss;
  ss<<"G"<<int(s)<<" " << endl;
  Message m(ss.str());
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  auto recep = mArduino->readMessage(true);
  info << recep << "\n";
  info<<"Done\n";
  return recep.get() =="1";
}

void Action::beltStraight(){
  Message m("BELTSTRAIGHT ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}
void Action::beltPull(){
  Message m("BELTPULL ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}
void Action::beltStop(){
  Message m("BELTSTOP ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}
void Action::beltPush(){
  Message m("BELTPUSH ");
  mArduino->flush();
  m.send(*mArduino);
  info<<"Sent\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<mArduino->readMessage(true)<<"\n";
  info<<"Done\n";
}

