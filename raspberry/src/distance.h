#pragma once

#include <iostream>
#include <cmath>

/// \brief Encapsulate a distance with conversion and all.
class Distance {
  public:
    constexpr Distance(double m) :
      mMeters(m) {
    }

    constexpr inline double meters() const noexcept {
      return mMeters;
    }

    inline double millimeters() const noexcept {
      return mMeters*1000;
    }

    inline double centimeters() const noexcept {
      return mMeters*100;
    }

    inline void operator+=(Distance d) {
      mMeters += d.meters();
    }

    inline void operator-=(Distance d) {
      mMeters -= d.meters();
    }

    inline void operator*=(double d) {
      mMeters *= d;
    }

    inline bool zero() {
      return (mMeters==0);
    }

    inline bool pos(){
      return mMeters > 0;
    }

    inline bool neg(){
      return mMeters < 0;
    }

    inline bool operator<(const Distance d){
      return mMeters < d.mMeters;
    }

    inline bool operator>(const Distance d){
      return mMeters > d.mMeters;
    }

    inline int sign(){
      if(pos()) return 1;
      else if(neg()) return -1;
      else return 0;
    }

  private:
    double mMeters;
};

inline Distance operator*(double a, Distance b) {
  return Distance(a*b.meters());
}

inline Distance operator*(Distance a, double b) {
  return Distance(a.meters()*b);
}

inline Distance operator+(Distance a, Distance b) {
  return Distance(a.meters()+b.meters());
}

constexpr inline Distance operator-(Distance a, Distance b) {
  return Distance(a.meters()-b.meters());
}

inline Distance operator-(Distance a){
  return Distance(-a.meters());
}

inline double operator/(Distance a, Distance b){
  return a.meters()/b.meters();
}

inline bool operator==(Distance a, Distance b) {
  return (a.meters()==b.meters());
}

constexpr Distance operator"" _mm(long double mm) {
  return Distance(0.001d * mm);
}

constexpr Distance operator"" _cm(long double cm) {
  return Distance(0.01d * cm);
}

constexpr Distance operator"" _m(long double m) {
  return Distance(m);
}

constexpr Distance operator"" _mm(unsigned long long int mm) {
  return Distance(0.001d * mm);
}

constexpr Distance operator"" _cm(unsigned long long int cm) {
  return Distance(0.01d * cm);
}

constexpr Distance operator"" _m(unsigned long long int m) {
  return Distance(m);
}

inline std::ostream& operator<<(std::ostream& out, Distance d){
  if(std::abs(d.meters()) > 1.0){
    out << d.meters() << "m";
  }
  else {
    out << d.centimeters() << "cm";
  }
  return out;
}
