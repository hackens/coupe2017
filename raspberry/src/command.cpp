#include <iostream>
#include <sstream>
#include "command.h"
#include "motion.h"
#include "actions.h"
#include "robot.h"
#include <unistd.h>
#include "task.h"

using namespace std;

static bool oneTime = true;

void cmd(int argc, char** argv){
  vector<string> vs;
  for(int i = 1 ; i < argc ; ++i){
    vs.push_back(argv[i]);
  }
  cmd(vs);

}


static double readD(string s){
  stringstream ss(s);
  double d;
  ss >> d;
  return d;
}

void cmd(std::vector<std::string> vs){
  string cmd = vs[0];
  if(cmd == "help"){
    cout << "[CMD] Help robot control :" << endl;
    cout << "f   for forward + distance (float) in cm" << endl;
    cout << "fs   for forward slow + distance (float) in cm" << endl;
    cout << "t   for turn + angle (float) in degrees" << endl;
    cout << "g   for grab" << endl;
    cout << "r   for release" << endl;
    cout << "bs  for belt straight" <<endl;
    cout << "d   for detach all" << endl;
    cout << "gs  for grab sequence" << endl;
    cout << "gas for grab all sequence" << endl;
    cout << "rs  for release sequence" << endl;
    cout << "ras  for release all sequence" << endl;
    cout << "bpl for belt pull" << endl;
    cout << "bstp for belt stop" << endl;
    cout << "bpsh for belt push" << endl;
    cout << "li  for lift" << endl;
    cout << "lo  for lower" << endl;
    cout << "lk  for lock" << endl;
    cout << "ul  for unlock" << endl;
    cout << "wh  for printing current position" << endl;
    cout << "reset for reseting position" << endl;
    cout << "gt  for going to given position" << endl;
    cout << "gfh for grabbing a cylinder at a given position" << endl;
    cout << "gafh for grabbing all cylinders at a given position" << endl;
    cout << "rd for releasing in direction (3args pos and dir)" << endl;
    cout << "gg for going on the graph from one pos to the other" << endl;
    cout << "int for interactive command line" << endl;
    cout << "quit for quitting interactive command line" << endl;
  }
  else if(cmd == "f"){
    if(vs.size() <= 1){
      cout << "[CMD] turn command without value" << endl;
      return;
    }
    double d = readD(vs[1]);
    cout << "[CMD] Moving of " << d << "cm" << endl;
    Mobility::Motion m(d/100,0);
    m.execute();
  }
  else if(cmd == "fs"){
    if(vs.size() <= 1){
      cout << "[CMD] turn command without value" << endl;
      return;
    }
    double d = readD(vs[1]);
    cout << "[CMD] Moving of " << d << "cm" << endl;
    Mobility::Motion m(d/100,0,true);
    m.execute();
  }

  else if (cmd == "t"){
    if(vs.size() <= 1){
      cout << "[CMD] turn command without value" << endl;
      return;
    }
    double d = readD(vs[1]);
    cout << "[CMD] Turning of " << d << "°" << endl;
    Mobility::Motion m(0,d/180*3.14159);
    m.execute();
  }
  else if(cmd == "g"){
    cout << "[CMD] Grabbing" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.grab();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "r"){
    cout << "[CMD] Releasing" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.release();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "d"){
    cout << "[CMD] Detaching" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.detachAll();
  }
  else if(cmd == "gs"){
    cout << "[CMD] Grabbing Seq" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.grabSeq();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "gas"){
    cout << "[CMD] Grabbing all Seq" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.grabAllSeq();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "rs"){
    cout << "[CMD] Releasing Seq" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.releaseSeq();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "ras"){
    cout << "[CMD] Releasing All Seq" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.releaseAllSeq();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "bpl"){
    cout << "[CMD] Pulling belt" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.beltPull();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "bpsh"){
    cout << "[CMD] Pushing Belt" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.beltPush();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "bstp"){
    cout << "[CMD] Stopping Belt" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.beltStop();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "li"){
    cout << "[CMD] Lift" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.lift();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "lo"){
    cout << "[CMD] Lower" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.lower();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "lk"){
    cout << "[CMD] Lower" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.lock();
    if(oneTime)Robot::sAction.detachAll();
  }
  else if(cmd == "ul"){
    cout << "[CMD] Lower" << endl;
    if(oneTime)usleep(1500000);
    Robot::sAction.unlock();
    if(oneTime)Robot::sAction.detachAll();
  }

  else if(cmd == "wh"){
    cout << "[CMD] Position : ";
    cout << Robot::sPosition << " and orientation : "
         << Robot::sOrientation/3.14159*180 << endl;
  }
  else if(cmd == "reset"){
    cout << "[CMD] Reset" << endl;
    Robot::sPosition = Vector2();
    Robot::sOrientation = 0;
  }
  else if(cmd == "gt"){
    if(vs.size() <= 3){
      cout << "Goto without enhough arguments" << endl;
      return;
    }
    double d1 = readD(vs[1]), d2 = readD(vs[2]);
    cout << "[CMD] Going To" << d1 << " " << d2 << endl;
    Behavior::DirectGoTo gt("CMD gt",Vector2(Distance(d1/100),Distance(d2/100)));
    gt.execute();
  }
  else if(cmd == "gfh"){
    if(vs.size() <= 3){
      cout << "Grab From Here without enhough arguments" << endl;
      return;
    }
    double d1 = readD(vs[1]), d2 = readD(vs[2]);
    cout << "[CMD] Grabbing at" << d1 << " " << d2 << endl;
    Behavior::GrabFromHere gfh("CMD gfh",Vector2(Distance(d1/100),Distance(d2/100)));
    gfh.execute();
  }
  else if(cmd == "gafh"){
    if(vs.size() <= 3){
      cout << "Grab All From Here without enhough arguments" << endl;
      return;
    }
    double d1 = readD(vs[1]), d2 = readD(vs[2]);
    int num = 1;
    if(vs.size() == 5) num = readD(vs[3]);
    cout << "[CMD] Grabbing all at" << d1 << " " << d2 << endl;
    Behavior::GrabFromHere gafh("CMD gafh",Vector2(Distance(d1/100),Distance(d2/100)),true,num);
    gafh.execute();
  }
  else if(cmd == "rd"){
    if(vs.size() <= 4){
      cout << "Release Dir without enhough arguments" << endl;
      return;
    }
    double d1 = readD(vs[1]), d2 = readD(vs[2]), d3 = readD(vs[3]);
    cout << "[CMD] Releasing at" << d1 << " " << d2 << " directing at" << d3 << endl;
    Behavior::ReleaseDir rd("CMD rd",Vector2(Distance(d1/100),Distance(d2/100)),d3 /180 * 3.14159);
    rd.execute();
  }
  else if(cmd == "gg"){
    if(vs.size() <= 3){
      cout << "Goto Graph without enhough arguments" << endl;
      return;
    }
    if(!Robot::sGraph.in(vs[1])) {
      cout << vs[1] << " is not in the graph !" << endl;
      return;
    }
    if(!Robot::sGraph.in(vs[2])) {
      cout << vs[2] << " is not in the graph !" << endl;
      return;
    }
    Behavior::GotoGraph gg("CMD gg",Robot::sGraph,
                           Robot::sGraph[vs[1]].num(), Robot::sGraph[vs[2]].num());
    gg.execute();
  }

  else if(cmd =="int"){
    if(oneTime)usleep(1500000);
    oneTime = false;
    inter();
  }
  else if(cmd == "quit"){
    exit(0);
  }
  else{
    cout << "Unknown command " << cmd <<endl;
  }

}

void inter(){
  while(true){
    cout << "\x1b[32mrobot\x1b[m$ ";
    char buffer[100];
    cin.getline(buffer,100);
    if(!cin){
      cout << "Bye !" << endl;
      exit(0);
    }
    string s = buffer;
    stringstream ss(s);
    vector<string> vs;
    while(ss){
      string s;
      ss >> s;
      vs.push_back(s);
    }
    cmd(vs);
  }
}
