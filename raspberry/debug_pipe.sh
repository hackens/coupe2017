#!/bin/bash

exitfn() {
  trap SIGINT
  echo "Closing pipe"
  rm -f gui_fifo
}

trap "exitfn" INT

mkfifo gui_fifo; tail -f gui_fifo; rm -f gui_fifo

trap SIGINT
