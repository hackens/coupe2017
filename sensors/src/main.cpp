#include <iostream>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sstream>
#include <deque>

#include "gui_interface.h"
#include "com.h"
#include "robot.h"

using namespace std;
using namespace Com;

Serial* serial;

int main() {
  try {
    struct stat st;
    if (!stat("gui_fifo",&st) && S_ISFIFO(st.st_mode)) {
      std::cout<<"Connecting to gui_fifo..."<<std::endl; 
      gui_fifo = make_shared<std::ofstream>("gui_fifo",std::ofstream::out);
      std::cout<<"fifo is "<<gui_fifo.get()<<std::endl;
    }
    else {
      std::cerr<<"Could not find GUI fifo"<<std::endl;
    }

    //Arduino boot sequence
    int time=0;

    while (time<500) {
      usleep(200000); time+=200;
      usleep(200000); time+=200;
    }

    Robot::sStepper.flush();
    Robot::sServos.flush();

    while (true) {
      std::stringstream packet;
      for (int i=1; i<=6; ++i) {
        std::stringstream ss;
        ss<<"G"<<i<<" ";
        Message m(ss.str());
        m.send(Robot::sServos);
        Robot::sServos.readMessage(true);
        int v = (Robot::sServos.readMessage(true).get()=="1");
        packet<<i<<" -> "<<v<<"\n";
      }
      Message m("RADAR");
      m.send(Robot::sStepper);
      packet<<Robot::sStepper.readMessage(true).get()<<"\n";

      std::cout<<packet.str()<<std::endl;
      packet.clear();

      usleep(200000);
      for (int i=0; i<0; i++) {
        std::cout<<std::endl;
      }
    }
  }
  catch (std::string s) {
    std::cerr << "[ERROR]" << s << "\n";
  }

  return 0;
}

