#pragma once

#include "com.h"

/// Contains all static variables
struct Robot{
  static Com::Serial sStepper;
  static Com::Serial sServos;
};
