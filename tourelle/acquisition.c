#include "acquisition.h"
#include <stdio.h>

#define DEBUG

Parameters acquire() {
#ifndef DEBUG
  for (int i=0; i<1000; i++) {
    buffer[i] = digitalRead(PIN_ROT_SENSOR);
  }
#else
  for (int i=0; i<1000; i++) {
    buffer[i] = 0;
  }
  buffer[60]=1; buffer[60+1]=1; buffer[60+2]=1;
  buffer[660]=1; buffer[660+1]=1; buffer[660+2]=1;
  buffer[68]=1;
#endif

  const int debounceLength=2;
  int rotPeriod=-1;
  {
    int pulseStart=0;
    int pulseMiddle=-1;
    int state=0;  //0=init, 1=started, 2=in_pulse
    for (int i=0; i<1000; i++) {
      switch(state) {
        case 0:
          state=1;
          break;
        case 1:
          if (buffer[i]) {
            pulseStart=i;
            state=2;
          }
          break;
        case 2:
          if (!buffer[i]) {
            state=1;
            int pulseLength=i-pulseStart;
            if (pulseLength>debounceLength) {
              int curPulseMid = (i+pulseStart)/2;
              if (pulseMiddle==-1) {
                pulseMiddle=curPulseMid;
              }
              else {
                rotPeriod=curPulseMid-pulseMiddle;
                state=3;
              }
            }
          }
          break;
      }
    }
  }
  printf("rotPeriod = %d\n",rotPeriod);


#ifndef DEBUG
  while (digitalRead(PIN_ROT_SENSOR));
  while (!digitalRead(PIN_ROT_SENSOR));
  for (int i=0; i<1000; i++) {
    buffer[i] = digitalRead(PIN_LASER_SENSOR);
  }
#else
  for (int i=0; i<1000; i++) {
    buffer[i] = 0;
  }
  buffer[100]=1;
  buffer[121]=1; buffer[121+1]=1; buffer[121+2]=1;
  buffer[334]=1; buffer[334+1]=1; buffer[334+2]=1;
  buffer[418]=1; buffer[418+1]=1; buffer[418+2]=1;
  buffer[600+121]=1; buffer[600+121+1]=1; buffer[600+121+2]=1;
  buffer[600+334]=1; buffer[600+334+1]=1; buffer[600+334+2]=1;
#endif

  //assert(!buffer[0]);
  int pulse1=-1, pulse2=-1, pulse3=-1;
  {
    int pulseStart=0;
    int state=1;  //1=started, 2=in_pulse
    for (int i=0; i<rotPeriod; i++) {
      switch(state) {
        case 1:
          if (buffer[i]) {
            state=2;
            pulseStart=i;
          }
          break;
        case 2:
          if (!buffer[i]) {
            state=1;
            if (i-pulseStart>debounceLength) {
              int pulseMid=(i+pulseStart)/2;
              if (pulse1==-1)
                pulse1=pulseMid;
              else if (pulse2==-1)
                pulse2=pulseMid;
              else if (pulse3==-1)
                pulse3=pulseMid;
            }
          }
          break;
      }
    }
  }

  double v1_deg=((double)pulse1)*360.0/((double)rotPeriod);
  double v2_deg=((double)pulse2)*360.0/((double)rotPeriod);
  double v3_deg=((double)pulse3)*360.0/((double)rotPeriod);

  printf("Angles : %f deg\t%f deg\t%f deg\n",v1_deg,v2_deg,v3_deg);

  Parameters p;
  p.v1=v1_deg*3.141592/180.;
  p.v2=v2_deg*3.141592/180.;
  p.v3=v3_deg*3.141592/180.;

  return p;
}
