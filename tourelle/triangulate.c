#include "triangulate.h"
#include <math.h>

Result triangulate(Parameters p) {
  double v1=p.v1;
  double v2=p.v2;
  double v3=p.v3;
  double w=3.0;
  double h=2.0;
  double pi = 3.141592;

  double hpi = pi/2;
  double a1 = tan(v1-v2+(1/2)*pi);
  double a2 = tan(v2+hpi-v3);
  double h2 = h*h;
  double w2 = w*w;
  double a12 = a1*a1;
  double a22 = a2*a2;

  double x = -tan(v1-v2-atan((1/2)*h*(2*a1*a2+a12-1)/(w*a12+h*a2+h*a1+w))+hpi)*(4*h*w*a22*a1+2*h*w*a2*a12-2*h2*a22-3*h2*a2*a1+4*w2*a2*a1-h2*a12-6*h*w*a2-4*h*w*a1-4*w2)*h/(4*h2*a22+4*h2*a2*a1+h2*a12+4*w2*a12+8*h*w*a2+8*h*w*a1+h2+4*w2);

  double y = (2*(-24*a22*a1-12*a2*a12+8*a22-24*a1*a2+4*a12+36*a2+24*a1+36))/(16*a22+16*a1*a2+40*a12+48*a2+48*a1+40);

  double alpha = v1-atan((2*a1*a2+a12-1)/(3*a12+2*a2+2*a1+3));

  Result r;
  r.x=x;
  r.y=y;
  r.alpha=alpha;
  return r;
}
