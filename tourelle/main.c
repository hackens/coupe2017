#include <stdio.h>
#include "triangulate.h"
#include "acquisition.h"

int main() {
  double alpha = 1.3;
  double v1 = alpha + 0;
  double v2 = alpha + 1.00 + 3.14/2;
  double v3 = alpha + 0.58 + 3.14;

  Parameters p;
  p=acquire();

  Result r = triangulate(p);
  printf("%f %f %f\n",r.x,r.y,r.alpha);
}
