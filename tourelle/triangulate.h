#pragma once

typedef struct Result {
  double x,y,alpha;
} Result;

typedef struct Parameters {
  double v1, v2, v3;
} Parameters;

Result triangulate(Parameters p);
