

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}


const char DiodeIn = 3;
const char LaserIn = 2;
const int N = 500;
unsigned char bufferLaser[N];
unsigned char bufferDiode[N];
const double rotorSpeed = 5; // tr/s
const double deltat = (0.4/N)/rotorSpeed;
unsigned long deltatmus = deltat * 1000000;
unsigned int const nbCylinders = 2;
float angles[nbCylinders];
bool hasFirstPosition;

typedef unsigned ui;
typedef unsigned long ul;


bool getAngles();

void setup() {
  pinMode(5,OUTPUT);
  setPwmFrequency(5, 8); // f = 32000/8
  analogWrite(5,128);
  pinMode(DiodeIn,INPUT);
  pinMode(LaserIn,INPUT);
  Serial.begin(9600);
}



bool getAngles() {
  for(int i = 0; i<N; i++) {
    bufferLaser[i] = 0;
    bufferDiode[i] = 0;
  }
  while(!digitalRead(DiodeIn)) ;
  delay(1);
  while(!digitalRead(DiodeIn)) ;
  
  for(int i = 0; i<N; i++) {
    for(int j = 0; j<8; j++) {
      bufferLaser[i] |= digitalRead(LaserIn) << j;
      bufferDiode[i] |= digitalRead(DiodeIn) << j;
      bufferLaser[i] |= digitalRead(LaserIn) << j;
      bufferDiode[i] |= digitalRead(DiodeIn) << j;
    }
  }


  //We delete the groups that have less than minSize bits for the laser
  int minSize = 10;
  int temp = 0;
  for(int i = 0; i<8*N; i++) {
    if(((bufferLaser[i>>3] >> (i%8)) & 1) == 1) {
      temp++;
    }
    else if(temp != 0 && temp < minSize) {
      for(int k = i-temp; k<i; k++) {
        bufferLaser[k>>3] &= ~(1 << (k%8));
      }
      temp = 0;
    }
    else if(temp != 0) {
      temp = 0;
    }
  }
  
  //We delete the groups that have less than minSize bits for the diode
  /*minSize = 10;
  temp = 0;
  for(int i = 0; i<8*N; i++) {
    if(((bufferDiode[i>>3] >> (i%8)) & 1) == 1) {
      temp++;
    }
    else if(temp != 0 && temp < minSize) {
      for(int k = i-temp; k<i; k++) {
        bufferDiode[k>>3] &= ~(1 << (k%8));
      }
      temp = 0;
    }
    else if(temp != 0) {
      temp = 0;
    }
  }*/
    
    
  //We get the begining and the end of the signals
  int signals[(nbCylinders+1)*2];
  int nbset = 0;
  int i = 0;
  while((bufferLaser[i>>3] >> (i%8)) & 1 == 1) { //We skip the first signal, which might have been cutted
    i++;
  }
  for(; i<8*N && nbset < nbCylinders+1; i++) {
    if((bufferLaser[i>>3] >> (i%8)) & 1 == 1) {
      signals[nbset*2] = i;
      for(;i<8*N && (bufferLaser[i>>3] >> (i%8) & 1); i++) {
      }
      signals[nbset*2+1] = i;
      nbset++;
    }
  }
  
  
  //We get the begining of the first turn
  int offset = 0;
  float begturn = 0;
  int nbDiodeSignals = 0;
  temp = -1;
  int const finished = 50;
  for(int i = offset; i<8*N; i++) {
    if(((bufferDiode[i>>3] >> (i%8)) & 1) == 1) {
      begturn += i;
      temp = 0;
      nbDiodeSignals++;
    } else if(temp != -1) {
      temp++;
      if(temp > finished) {
        begturn /= (float)nbDiodeSignals;
        break;
      }
    }
  }
  
  
  //We get the end of the first turn
  offset = 200;
  float endturn = 0;
  nbDiodeSignals = 0;
  temp = -1;
  for(int i = offset; i<8*N; i++) {
    if(((bufferDiode[i>>3] >> (i%8)) & 1) == 1) {
      endturn += i;
      temp = 0;
      nbDiodeSignals++;
    } else if(temp != -1) {
      temp++;
      if(temp > finished) {
        endturn /= (float)nbDiodeSignals;
        break;
      }
    }
  }
  
  
  if(nbset < nbCylinders) {
    Serial.println("Error : Did not detect enough cylinders");
    return false;
  }
  
  //We check if we found the wanted number of cylinders
  if(signals[(nbCylinders-1)*2] > endturn) {
    Serial.println("Error : Did not detect enough cylinders");
    return false;
  }
  if(signals[nbCylinders*2] < endturn && signals[0] > 100) {
    Serial.println("Error : Detected too much cylinders");
    return false;
  }
  
  if(endturn - begturn < 2000) {
    Serial.println("Error : The turn was too fast");
    return false;
  }
  
  //We compute the angles relative to the diode
  for(int i = 0; i<nbCylinders; i++) {
    float center = (3.*(float)signals[i*2] + (float)signals[i*2+1]) / 4.;
    angles[i] = ((center-begturn) / (endturn-begturn)) * 2.*3.14159265;
  }
   
  return true;
}


void getNAngles(int nbAngles) {
  float angles2[nbCylinders];
  int i = 0;
  while(i < nbAngles) {
    bool result = getAngles();
    if(result) {
      i++;
      for(int i = 0; i<nbCylinders; i++) {
        angles2[i] += angles[i];
      }
    }
  }
  
  for(int i = 0; i<nbCylinders; i++) {
    angles2[i] /= nbAngles;
    angles[i] = angles2[i];
  }
}


void loop() {
  ul t = millis();
  getNAngles(15);
  Serial.print("Time : ");
  Serial.println(t);
  Serial.print("Time2 : ");
  Serial.println(millis());
  for(int i = 0; i<nbCylinders; i++) {
    Serial.println(angles[i]);
  }
  Serial.println();
}
