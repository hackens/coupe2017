#include "pmrbutton.h"
#include "runbutton.h"
#include "stdlib.h"
#include "globals.h"
#include "messages.pb.h"
#include "delimitedstream.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

PMRButton::~PMRButton() {
  delete mRB;
}

void PMRButton::onClickSafe() {
  mRB = new PMRDialog(" ",10,10,300,400);
  mRB->setZ(42);
  mRB->run("(cd ..; cd /home/pi/coupe2017 ; git pull && cd raspberry && make; sleep 2)2>&1");

  Globals::shouldDrawSplash = true;
}

bool PMRButton::draw(SDL_Surface* s) {
  Button::draw(s);
  if (mRB!=nullptr && mRB->isDone()) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

PMRButton::PMRDialog::~PMRDialog() {
}

void PMRButton::PMRDialog::update() {
  RunButton::update();
    //DEBUG
}

