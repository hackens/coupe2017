#include "button.h"
#include <algorithm>
#include <iostream>
#include "globals.h"

Button::Button(std::string t, int x, int y, int w, int h, SDL_Color bg):
    mFont(Globals::sFont),mT(t),mX(x),mY(y),mW(w),mH(h),mBG(bg) {
  SDL_Color c = {0,0,0,255};
  mSurface = TTF_RenderText_Blended(mFont,mT.c_str(),c);
  sButtons.push_back(this);
  std::sort(sButtons.begin(),sButtons.end(), [](Button* a, Button* b) -> bool {
        return a->mZ<b->mZ;
      });
}

Button::~Button() {
  SDL_FreeSurface(mSurface);
  sButtons.erase(std::remove(sButtons.begin(),sButtons.end(),this));
  Globals::shouldDrawSplash = true;
}

int Button::getZ() const {
  return mZ;
}

void Button::setZ(int z) {
  mZ=z;
  std::sort(sButtons.begin(),sButtons.end(), [](Button* a, Button* b) -> bool {
        return a->mZ<b->mZ;
      });
}

int Button::getX() const {
  return mX;
}

void Button::setX(int X) {
  mX = X;
}

int Button::getY() const {
  return mY;
}

void Button::setY(int Y) {
  mY = Y;
}

void Button::setFont(TTF_Font* f) {
  mFont=f;
}

void Button::setBackground(SDL_Color bg) {
  mBG = bg;
}

void Button::setText(std::string t) {
  SDL_Color c = {0,0,0,255};
  mT = t;
  SDL_FreeSurface(mSurface);
  mSurface = TTF_RenderText_Solid(mFont,mT.c_str(),c);
}

bool Button::draw(SDL_Surface* s) {
  SDL_Rect rOuter = {mX,mY,mW,mH};
  SDL_FillRect(s,&rOuter,SDL_MapRGB(s->format,0,0,0));
  SDL_Rect rInner = {mX+2,mY+2,mW-4,mH-4};
  SDL_FillRect(s,&rInner,SDL_MapRGB(s->format,mBG.r,mBG.g,mBG.b));

  SDL_Rect pos = {mX,mY,mW,mH};
  pos.x += (mW - mSurface->w)/2;
  pos.y += (mH - mSurface->h)/2;
  SDL_BlitSurface(mSurface,NULL,s,&pos);
  return false;
}

void Button::clicked(int x, int y) {
  int highestZ = -10000;
  Button* highestB = NULL;
  for (Button* b : sButtons) {
    if (b->mX<=x && x<=b->mX+b->mW && b->mY<=y && y<=b->mY+b->mH) {
      if (b->mZ > highestZ) {
        highestZ = b->mZ;
        highestB = b;
      }
    }
  }
  if (highestB != NULL) {
    std::cout<<"Clicked "<<highestB->mT<<std::endl;
    highestB->onClick();
  }
}

void Button::drawAll(SDL_Surface* s) {
  for (Button* b : sButtons) {
    if (b->draw(s))
      break;
  }

  //std::cout<<"DRAWALL"<<std::endl;

  bool ok=true;
  do {
    ok=true;
    for (Button* b : sButtons) {
      if (b->isMarkedForDeletion()) {
        std::cout<<"DELETED MARKED "<<b<<std::endl;
        ok=false;
        delete b;
        break;
      }
    }
  } while (!ok);
}

void Button::markForDeletion() {
  //std::cout<<"Marked "<<this<<" for deletion"<<std::endl;
  mDeleteMe=true;
}

bool Button::isMarkedForDeletion() {
  return mDeleteMe;
}

std::vector<Button*> Button::sButtons;

