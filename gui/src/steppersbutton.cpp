#include "steppersbutton.h"
#include <sstream>
#include <unistd.h>
#include <iostream>
#include "fakebutton.h"
#include "stdlib.h"
#include "globals.h"
#include "com.h"

using namespace Com;

SteppersButton::SteppersButton(std::string text, int x, int y, int w, int h) :
  Button(text,x,y,w,h) {
}

SteppersButton::~SteppersButton() {
  mRB->markForDeletion();
  delete mSerial;
  for (MessageButton* m : mButtons)
    m->markForDeletion();
  mButtons.clear();
}

void SteppersButton::onClick() {
  mRB = new FakeButton(" ",10,50,300,400);
  mRB->setZ(42);

  mSerial = new Serial("/dev/ttyACM0",9600);
  time(&mSerialBegin);

  Globals::shouldDrawSplash = true;
}

bool SteppersButton::draw(SDL_Surface* s) {
  if (update())
    return true;
  Button::draw(s);
  if (mRB!=nullptr && false) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

bool SteppersButton::update() {
  time_t end;
  time(&end);
  if (mRB && !mLoaded && difftime(end,mSerialBegin)>2.0) {
    mRB->setText("LOADED");
    mLoaded=true;

    MessageButton* m = new MessageButton("FWD  5",10+5,50+5,130,50,"FWD  5\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("FWD  100",10+5+135,50+5,130,50,"FWD  100\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("FWD  200",10+5,50+5+55,130,50,"FWD  200\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("FWD  500",10+5+135,50+5+55,130,50,"FWD  500\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);

    m = new MessageButton("RESET", 10+5,50+5+55*2,130,50,"RESET\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);

    mL = new GpioButton("L",10+5,50+5+55*5,130,50,20);
    mL->setZ(43);

    mR = new GpioButton("R",10+5+135,50+5+55*5,130,50,21);
    mR->setZ(43);


    return true;
  }
  return false;
}

SteppersButton::MessageButton::MessageButton(
  std::string text, int x, int y, int w, int h, std::string cmd)
  : Button(text,x,y,w,h), mCmd(cmd) {
}

void SteppersButton::MessageButton::onClick() {
  if (!mParent)
    return;
  mParent->mSerial->writeMessage(mCmd);
  std::cout<<"Sent "<<mCmd<<std::endl;
  std::cout<<mParent->mSerial->readMessage(true)<<"\n";
  std::cout<<mParent->mSerial->readMessage(true)<<"\n";
  usleep(50000);
}

void SteppersButton::MessageButton::setParent(SteppersButton* p) {
  mParent = p;
}

SteppersButton::GpioButton::GpioButton(
  std::string text, int x, int y, int w, int h, int g)
  : Button(text,x,y,w,h), mGpio(g) {
  onClick();
}

void SteppersButton::GpioButton::writeVal(std::string f, std::string val) {
  std::cout<<"Write "<<val.substr(0,val.size()-1)<<" to "<<f<<std::endl;
  std::ofstream s(f);
  s<<val;
  s.close();
}

void SteppersButton::GpioButton::onClick() {
  v = !v;
  std::stringstream pinNum;
  pinNum<<mGpio<<"\n";
  writeVal("/sys/class/gpio/export",pinNum.str());

  std::stringstream pinPath;
  pinPath<<"/sys/class/gpio/gpio"<<mGpio;
  writeVal(std::string(pinPath.str())+std::string("/direction"),"out\n");

  writeVal(std::string(pinPath.str())+std::string("/value"),v?"1\n":"0\n");
  if (v) {
    setBackground({255,0,0,255});
  }
  else {
    setBackground({0,255,0,255});
  }
}

