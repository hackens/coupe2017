#include "sensorsbutton.h"
#include <unistd.h>
#include <iostream>
#include "fakebutton.h"
#include "stdlib.h"
#include "globals.h"
#include "com.h"

using namespace Com;

SensorsButton::~SensorsButton() {
  mRB->markForDeletion();
  delete mSerial;
  delete mSteppers;
  for (WatchButton* m : mButtons)
    m->markForDeletion();
  mButtons.clear();
}

void SensorsButton::onClick() {
  mRB = new FakeButton(" ",10,50,300,400);
  mRB->setZ(42);

  mSerial = new Serial("/dev/ttyUSB0",9600);
  mSteppers = new Serial("/dev/ttyACM0",9600);
  time(&mSerialBegin);

  Globals::shouldDrawSplash = true;
}

bool SensorsButton::draw(SDL_Surface* s) {
  if (update())
    return true;
  Button::draw(s);
  if (mRB!=nullptr && false) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

bool SensorsButton::update() {
  time_t end;
  time(&end);
  if (mRB && !mLoaded && difftime(end,mSerialBegin)>2.0) {
    mRB->setText("LOADED");
    mLoaded=true;

    WatchButton* m = new WatchButton("G1 ",10+5,50+5,130,50,"G1 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("G2 ",10+5+135,50+5,130,50,"G2 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("G3 ",10+5,50+5+55,130,50,"G3 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("G4 ",10+5+135,50+5+55,130,50,"G4 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("G5 ",10+5,50+5+55*2,130,50,"G5 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("G6 ",10+5+135,50+5+55*2,130,50,"G6 \n");
    m->setZ(43);
    m->mDevice = mSerial;
    mButtons.push_back(m);
    m = new WatchButton("RADAR ",10+5,50+5+55*3,130+135,50,"RADAR\n");
    m->setZ(43);
    m->mDevice = mSteppers;
    mButtons.push_back(m);
    return true;
  }
  if (mLoaded) {
    for (WatchButton* b : mButtons) {
      b->mDevice->writeMessage(b->mCmd);
      std::cout<<"Sent "<<b->mCmd<<std::endl;
      if (b->mDevice == mSerial)
        std::cout<<b->mDevice->readMessage(true)<<"\n";
      std::string msg = b->mDevice->readMessage(true).get();
      if (msg=="1") {
        b->setBackground({255,0,0,255});
      }
      else {
        b->setBackground({0,255,0,255});
      }
      usleep(50000);
    }
  }
  return false;
}

SensorsButton::WatchButton::WatchButton(
  std::string text, int x, int y, int w, int h, std::string cmd)
  : FakeButton(text,x,y,w,h), mCmd(cmd) {
}

