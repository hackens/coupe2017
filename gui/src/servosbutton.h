#pragma once

#include "button.h"
#include "runbutton.h"
#include "com.h"
#include <time.h>
#include <stdio.h>

class FakeButton;

class ServosButton : public Button {
  public:
    ServosButton(std::string text, int x, int y, int w, int h);
    virtual ~ServosButton();

    virtual void onClick();
    virtual bool draw(SDL_Surface* s);

  protected:
    virtual bool update();

  private:
    class MessageButton : public Button {
      public:
        MessageButton(std::string text, int x, int y, int w, int h,
          std::string cmd);

        virtual void onClick();
        void setParent(ServosButton* p);
      
      private:
        std::string mCmd;
        ServosButton* mParent = nullptr;
    };

    FakeButton* mRB = nullptr;
    std::vector<MessageButton*> mButtons;
    Com::Serial* mSerial = nullptr;
    time_t mSerialBegin = 0;
    bool mLoaded=false;
};

