#pragma once

#include "button.h"
#include "runbutton.h"
#include "com.h"
#include <time.h>
#include <stdio.h>

class FakeButton;

class SteppersButton : public Button {
  public:
    SteppersButton(std::string text, int x, int y, int w, int h);
    virtual ~SteppersButton();

    virtual void onClick();
    virtual bool draw(SDL_Surface* s);

  protected:
    virtual bool update();

  private:
    class MessageButton : public Button {
      public:
        MessageButton(std::string text, int x, int y, int w, int h,
          std::string cmd);

        virtual void onClick();
        void setParent(SteppersButton* p);
      
      private:
        std::string mCmd;
        SteppersButton* mParent = nullptr;
    };

    class GpioButton: public Button {
      public:
        GpioButton(std::string text, int x, int y, int w, int h, int gpio);
        virtual void onClick();
        static void writeVal(std::string f, std::string val);
      private:
        int mGpio = 0;
        bool v = false;
    };

    FakeButton* mRB = nullptr;
    std::vector<MessageButton*> mButtons;
    Com::Serial* mSerial = nullptr;

    GpioButton* mL = nullptr;
    GpioButton* mR = nullptr;

    time_t mSerialBegin = 0;
    bool mLoaded=false;
};

