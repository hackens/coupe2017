#include "syncbutton.h"
#include "runbutton.h"
#include "stdlib.h"
#include "globals.h"


CmdButton::CmdButton(std::string text, int x, int y, int w, int h, std::string cmd) :
  Button(text,x,y,w,h), mCmd(cmd) {
}

CmdButton::~CmdButton() {
  delete mRB;
}

void CmdButton::onClick() {
  mRB = new RunButton(" ",10,10,300,400);
  mRB->setZ(42);
  mRB->run(mCmd);

  Globals::shouldDrawSplash = true;
}

bool CmdButton::draw(SDL_Surface* s) {
  Button::draw(s);
  if (mRB!=nullptr && mRB->isDone()) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}


SyncButton::~SyncButton() {
  delete mRB;
}

void SyncButton::onClick() {
  mRB = new RunButton(" ",10,10,300,400);
  mRB->setZ(42);
  mRB->run("sync");

  Globals::shouldDrawSplash = true;
}

bool SyncButton::draw(SDL_Surface* s) {
  Button::draw(s);
  if (mRB!=nullptr && mRB->isDone()) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

