#include "globals.h"
#include <SDL.h>
#include <SDL_ttf.h>

const int Globals::cScreenWidth=320;
const int Globals::cScreenHeight=480;
const int Globals::cScreenBpp=32;

bool Globals::shouldDrawSplash = true;

SDL_Window* Globals::sWindow = NULL;
SDL_Surface* Globals::sScreen = NULL;
SDL_Surface* Globals::sSplash = NULL;
SDL_Renderer* Globals::sRenderer = NULL;
TTF_Font* Globals::sFont = NULL;
TTF_Font* Globals::sFontMono = NULL;
bool Globals::isBlue = true;

