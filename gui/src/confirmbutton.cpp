#include "confirmbutton.h"
#include "fakebutton.h"

void ConfirmButton::onClick() {
  if (mDialog)
    return;
  mDialog = new FakeButton(" ",20,140,280,200);
  mDialog->setZ(100);
  mConfirm = new OkButton("Ok",40,140+20,110,160);
  mConfirm->setBackground({255,0,0,255});
  mConfirm->mParent=this;
  mConfirm->setZ(101);
  mCancel = new CancelButton("Cancel",40+110+20,140+20,110,160);
  mCancel->mParent=this;
  mCancel->setZ(101);
}

void ConfirmButton::closeDialog(bool ok) {
  if (!mDialog) {
    std::cerr<<"Dialog already marked for deletion"<<std::endl;
    return;
  }
  mDialog->markForDeletion();
  mDialog=nullptr;
  mConfirm->markForDeletion();
  mConfirm=nullptr;
  mCancel->markForDeletion();
  mCancel=nullptr;
  if (ok) {
    onClickSafe();
  }
}

