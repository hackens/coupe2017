#include "colorbutton.h"
#include <iostream>
#include <fcntl.h>
#include "globals.h"



ColorButton::ColorButton(std::string t, int x, int y, int w, int h, bool* boolptr, SDL_Color cfalse, SDL_Color ctrue):
    Button(t,x,y,w,h,*boolptr ? ctrue : cfalse), mBGTrue(ctrue), mBGFalse(cfalse){
    valueptr = boolptr;
    Button::setZ(5);
}


void ColorButton::onClick() {
    *valueptr = !(*valueptr);
    mBG = *valueptr ? mBGTrue : mBGFalse;
}

