#include "runbutton.h"

#include <SDL_ttf.h>
#include "globals.h"
#include <iostream>
#include <fcntl.h>

RunButton::RunButton(std::string t, int x, int y, int w, int h, SDL_Color bg, SDL_Color tc):
  Button(t,x,y,w,h,bg), mTC(tc),mBuffer() {
  Button::setFont(Globals::sFontMono);
  TTF_SizeText(mFont,"a",&mCharWidth,&mCharHeight);
  mBufferHeight = (h-8) / (mCharHeight + 2);
  mBufferWidth = (w-8) / mCharWidth;
  Button::setZ(5);
}

void RunButton::setLog(std::string f) {
  mFile.open(f);
}

RunButton::~RunButton() {
  if (mFile) {
    mFile.close();
  }
  if(mPipe != nullptr) {
    pclose(mPipe);
  }
}

bool RunButton::draw(SDL_Surface* s) {
  update();
  Button::draw(s);
  int w = mCharWidth * mBufferWidth;
  int h = mCharHeight + 2;
  int x = 4 + Button::getX();
  SDL_Rect pos = {x,0,w,h};
  SDL_Surface* textSurface;
  for(unsigned int i = 0; i<mBuffer.size(); i++) {
    int y = i*(mCharHeight+2) + 4 + Button::getY();
    pos.y = y;
    textSurface = TTF_RenderText_Solid(mFont,
      mBuffer[i].c_str(),mTC);
    SDL_BlitSurface(textSurface,NULL,s,&pos);
    SDL_FreeSurface(textSurface);
  }
  return false;
}

void RunButton::run(std::string command) {
  if(mPipe != nullptr) {
    pclose(mPipe);
    mPipe = nullptr;
  }
  mPipe = popen(command.c_str(),"r");
  int fd = fileno(mPipe);
  int flags = fcntl(fd, F_GETFL, 0);
  flags |= O_NONBLOCK;
  fcntl(fd, F_SETFL, flags);
}

bool RunButton::isDone() const {
  return mDone;
}


void RunButton::update() {
  if(mPipe == nullptr) {
    return;
  }

  char readBuffer[mBufferWidth];
  while(fgets(readBuffer,mBufferWidth,mPipe) != NULL) {
    std::cout << readBuffer;
    std::string temp = readBuffer;
    if (mFile.is_open()) {
      mFile<<temp;
      mFile.flush();
    }
    if((int)mBuffer.size() == mBufferHeight) {
      mBuffer.pop_front();
    }
    mBuffer.push_back(temp.substr(0,temp.size()-1));
  }

  if(feof(mPipe)) {
    mDone=true;
    mPipe = nullptr;
  }
}

