#include "servosbutton.h"
#include <unistd.h>
#include <iostream>
#include "fakebutton.h"
#include "stdlib.h"
#include "globals.h"
#include "com.h"

using namespace Com;

ServosButton::ServosButton(std::string text, int x, int y, int w, int h) :
  Button(text,x,y,w,h) {
}

ServosButton::~ServosButton() {
  mRB->markForDeletion();
  delete mSerial;
  for (MessageButton* m : mButtons)
    m->markForDeletion();
  mButtons.clear();
}

void ServosButton::onClick() {
  mRB = new FakeButton(" ",10,50,300,400);
  mRB->setZ(42);

  mSerial = new Serial("/dev/ttyUSB0",9600);
  time(&mSerialBegin);

  Globals::shouldDrawSplash = true;
}

bool ServosButton::draw(SDL_Surface* s) {
  if (update())
    return true;
  Button::draw(s);
  if (mRB!=nullptr && false) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

bool ServosButton::update() {
  time_t end;
  time(&end);
  if (mRB && !mLoaded && difftime(end,mSerialBegin)>2.0) {
    mRB->setText("LOADED");
    mLoaded=true;

    MessageButton* m = new MessageButton("GRAB",10+5,50+5,130,50,"GRAB \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("RELEASE",10+5+135,50+5,130,50,"RELEASE \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("GRABSEQUENCE",10+5,50+5+55,130,50,"GRABSEQUENCE \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("RELEASESEQUENCE",10+5+135,50+5+55,130,50,"RELEASESEQUENCE \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("LOCK",10+5,50+5+55*2,130,50,"LOCK \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("UNLOCK",10+5+135,50+5+55*2,130,50,"UNLOCK \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("LIFT",10+5,50+5+55*3,130,50,"LIFT \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("LOWER",10+5+135,50+5+55*3,130,50,"LOWER \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("LOCK_ROCKET",10+5,50+5+55*4,130,50,"LOCK_ROCKET \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("LAUNCH_ROCKET",10+5+135,50+5+55*4,130,50,"LAUNCH_ROCKET \n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("GRABALLSEQUENCE",10+5,50+5+55*5,130,50,"GRABALL\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    m = new MessageButton("RELEASEALLSEQUENCE",10+5+135,50+5+55*5,130,50,"RELEASEALLSEQUENCE\n");
    m->setZ(43);
    m->setParent(this);
    mButtons.push_back(m);
    return true;
  }
  return false;
}

ServosButton::MessageButton::MessageButton(
  std::string text, int x, int y, int w, int h, std::string cmd)
  : Button(text,x,y,w,h), mCmd(cmd) {
}

void ServosButton::MessageButton::onClick() {
  if (!mParent)
    return;
  mParent->mSerial->writeMessage(mCmd);
  std::cout<<"Sent "<<mCmd<<std::endl;
  std::cout<<mParent->mSerial->readMessage(true)<<"\n";
  std::cout<<mParent->mSerial->readMessage(true)<<"\n";
  usleep(50000);
}

void ServosButton::MessageButton::setParent(ServosButton* p) {
  mParent = p;
}

