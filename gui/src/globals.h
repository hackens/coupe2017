#pragma once
#include <SDL.h>
#include <SDL_ttf.h>

class Globals {
  public:
    static const int cScreenWidth;
    static const int cScreenHeight;
    static const int cScreenBpp;

    static bool shouldDrawSplash;

    static SDL_Window* sWindow;
    static SDL_Surface* sScreen;
    static SDL_Surface* sSplash;
    static SDL_Renderer* sRenderer;
    static TTF_Font* sFont;
    static TTF_Font* sFontMono;
    static bool isBlue;
};
