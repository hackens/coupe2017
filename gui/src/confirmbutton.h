#pragma once
#include <iostream>
#include "button.h"

class ConfirmButton : public Button {
  public:
    using Button::Button;
    virtual ~ConfirmButton() {}

    void onClick();
    virtual void onClickSafe() = 0;

  private:
    class OkButton : public Button {
      public:
        using Button::Button;
        ConfirmButton* mParent=NULL;
        void onClick() {
          std::cout<<"Clicked ok"<<std::endl;
          mParent->closeDialog(true);
        }
    };

    class CancelButton : public Button {
      public:
        using Button::Button;
        ConfirmButton* mParent=NULL;
        void onClick() {
          std::cout<<"Clicked cancel"<<std::endl;
          mParent->closeDialog(false);
        }
    };

    void closeDialog(bool ok);

    Button* mDialog=NULL;
    OkButton* mConfirm=NULL;
    CancelButton* mCancel=NULL;
};

