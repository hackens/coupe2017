#pragma once
#include <SDL.h>
#include <vector>
#include "SDL_ttf.h"
#include <string>
#include "globals.h"

class Button {
  public:
    Button(std::string t, int x, int y, int w, int h, SDL_Color bg = {200,200,200,255});

    virtual ~Button();

    int getZ() const;
    void setZ(int z);

    int getX() const;
    void setX(int X);

    int getY() const;
    void setY(int Y);

    void setFont(TTF_Font* f);

    void setBackground(SDL_Color bg);
    void setText(std::string t);

    virtual bool draw(SDL_Surface* s);

    virtual void onClick() = 0;

    static void clicked(int x, int y);
    static void drawAll(SDL_Surface* s);

    virtual void markForDeletion();
    virtual bool isMarkedForDeletion();

  protected:
    TTF_Font* mFont = nullptr;
    bool mDeleteMe = false;
    std::string mT;
    int mX, mY, mW, mH;
    SDL_Surface* mSurface;
    int mZ=0;
    SDL_Color mBG;

    static std::vector<Button*> sButtons;
};

bool operator<(const Button& l, const Button& r);

