#include <iostream>
#include <sstream>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include "SDL_ttf.h"
#include <string>
#include "globals.h"
#include "button.h"
#include "confirmbutton.h"
#include "pmrbutton.h"
#include "syncbutton.h"
#include "sensorsbutton.h"
#include "competitionbutton.h"
#include "servosbutton.h"
#include "steppersbutton.h"
#include "colorbutton.h"

class ExitButton : public ConfirmButton {
  using ConfirmButton::ConfirmButton;

  void onClickSafe() {
    std::cout<<"Exiting..."<<std::endl;
    exit(0);
  }
};

int main() {
  SDL_Init(SDL_INIT_VIDEO);
  IMG_Init(IMG_INIT_PNG);
  TTF_Init();

  Globals::sWindow = SDL_CreateWindow("Robot GUI",0,0,Globals::cScreenWidth,Globals::cScreenHeight,0);
  SDL_RaiseWindow(Globals::sWindow);
  Globals::sFont = TTF_OpenFont("DroidSansMono.ttf",15);
  Globals::sFontMono = TTF_OpenFont("DroidSansMono.ttf",11);

  Globals::sScreen = SDL_GetWindowSurface(Globals::sWindow);
  Globals::sSplash = IMG_Load("hackens.png");

  ExitButton reboot("Exit",5,5,150,80,{255,0,0,255});
  PMRButton pmrbutton("PMR",165,5,150,80);
  SyncButton syncbutton("Sync",5,10+80,150,80);
  SensorsButton sensorsbutton("Sensors",165,10+80,150,80);
  CompetitionButton competitionbutton("Competition",5,5+2*85,150,80);
  SteppersButton steppersbutton("Steppers",165,5+2*85,150,80);
  ServosButton servosbutton("Servos ",5,5+3*85,150,80);
  ColorButton teambutton("Team",5,5+4*85,310,80,&Globals::isBlue);



  bool touchEventsWork=false;
  bool quit=false;
  int i=0;
  while (!quit) {
    SDL_Event evt;
    if (SDL_PollEvent(&evt) != 0) {
      //std::cout<<"In evt loop, got evt "<<evt.type<<std::endl;
      if (evt.type == SDL_QUIT) {
        quit=true;
      }
      if (evt.type == SDL_MOUSEBUTTONDOWN || evt.type == SDL_FINGERDOWN)
        touchEventsWork=true;
      if (evt.type == SDL_MOUSEBUTTONDOWN || evt.type == SDL_FINGERDOWN || (!touchEventsWork && evt.type==SDL_MOUSEMOTION) /*DEBUG*/) {
        int x,y;
        SDL_GetMouseState(&x,&y);
        std::cout<<"touch "<<x<<" "<<y<<std::endl;
        Button::clicked(x,y);
      }
    }
    
    if(true || Globals::shouldDrawSplash) {
      SDL_BlitSurface(Globals::sSplash,NULL,Globals::sScreen,NULL);
      Globals::shouldDrawSplash = false;
    }
    Button::drawAll(Globals::sScreen);
    SDL_UpdateWindowSurface(Globals::sWindow);

    ++i;
  }

  SDL_FreeSurface(Globals::sSplash);
  SDL_DestroyWindow(Globals::sWindow);
  TTF_CloseFont(Globals::sFont);
  TTF_CloseFont(Globals::sFontMono);

  TTF_Quit();
  IMG_Quit();
  SDL_Quit();

  return EXIT_SUCCESS;
}

