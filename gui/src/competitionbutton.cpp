#include "competitionbutton.h"
#include "runbutton.h"
#include "stdlib.h"
#include "globals.h"
#include "messages.pb.h"
#include "delimitedstream.h"

#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "globals.h"

CompetitionButton::~CompetitionButton() {
  delete mRB;
}

void CompetitionButton::onClickSafe() {
  mRB = new CompetitionDialog(" ",0,0,320,150);
  mRB->init();
  mRB->setZ(42);
  mRB->setLog("competition.log");
  if(Globals::isBlue) {
      mRB->run("(cd ../raspberry ; cd /home/pi/coupe2017/raspberry ; ./raspi blue; sleep 2)2>&1");
  }
  else {
      mRB->run("(cd ../raspberry ; cd /home/pi/coupe2017/raspberry ; ./raspi yellow; sleep 2)2>&1");
  }

  Globals::shouldDrawSplash = true;
}

bool CompetitionButton::draw(SDL_Surface* s) {
  Button::draw(s);
  if (mRB!=nullptr && mRB->isDone()) {
    mRB->markForDeletion();
    mRB=nullptr;
  }
  return false;
}

CompetitionButton::CompetitionDialog::~CompetitionDialog() {
  if (mFifo.get()) {
    mFifo.get()->close();
    chdir("../raspberry");
    chdir("/home/pi/coupe2017/raspberry");
    remove("gui_fifo");
    system("sudo killall raspivid\n");
  }
}

void CompetitionButton::CompetitionDialog::init() {
  mAbort.setParent(this);

  mVideoPid = system("cd /var/www/html ; (./video.sh &) ; return $!");
  std::cout<<"Got pid "<<mVideoPid<<std::endl;
}

void CompetitionButton::CompetitionDialog::update() {
  RunButton::update();
    //DEBUG
  
  if (!mFifo.get()->is_open()) {
    std::cout<<"not open"<<std::endl;
    return;
  }

  std::string message = mFifo.readDelimited();
  if (message.size()>0) {
    Position p;
    bool b = p.ParseFromString(message);
    if (b)
      std::cout<<"READ POSITION !!! "<<p.x()<<" "<<p.y()<<" "<<p.heading()<<std::endl;
      mPos.setPos(p.x(),p.y(),p.heading());
      mSchema.setPos(p.x(),p.y(),p.heading());
  }
}

void CompetitionButton::CompetitionDialog::run(std::string command) {
  if (mFifo.get()->is_open())
    return;

  //Create FIFO
  chdir("../raspberry");
  chdir("/home/pi/coupe2017/raspberry");
  int success;

  success = remove("gui_fifo");
  if (success==0)
    std::cerr<<"Deleted existing FIFO"<<std::endl;

  success = mknod("gui_fifo", S_IRUSR | S_IWUSR | S_IFIFO, 0);
  if (success!=0)
    std::cerr<<"Could not create FIFO"<<std::endl;

  std::cout<<"Running command"<<std::endl;
  RunButton::run(command);

  std::cout<<"Opening FIFO"<<std::endl;
  //Open FIFO
  mFifo.get()->open("gui_fifo",std::ifstream::in);
  if (!mFifo.get()->is_open()) {
    std::cerr<<"Could not open FIFO"<<std::endl;
    remove("gui_fifo");
  }
}

void CompetitionButton::CompetitionDialog::abort() {
  system("sudo killall -9 raspi");

  std::stringstream ss;
  ss<<"sudo killall raspivid\n";
  system(ss.str().c_str());
}

void CompetitionButton::CompetitionDialog::AbortButton::onClickSafe() {
  mParent->abort();
}

CompetitionButton::CompetitionDialog::AbortButton::AbortButton() :
  ConfirmButton("ABORT",5,155,100,50) {
  setZ(42);
}

void CompetitionButton::CompetitionDialog::AbortButton::setParent(
  CompetitionDialog* p) {
  mParent=p;
}

CompetitionButton::CompetitionDialog::BGButton::BGButton() :
  FakeButton(" ",0,0,320,480) {
  setZ(1);
}


CompetitionButton::CompetitionDialog::PosButton::PosButton() :
  FakeButton("?",110,155,320-110-10,50) {
  setZ(42);
}

void CompetitionButton::CompetitionDialog::PosButton::setPos(
  double x, double y, double h) {
  std::stringstream ss;
  ss<<(int)(x*1000)<<" "<<(int)(y*1000)<<" "<<(int)(h*180./3.1415);
  setText(ss.str());
}

CompetitionButton::CompetitionDialog::TerrainSchema::TerrainSchema() :
  FakeButton(" ",5,210,320-10,(310-10)*2/3) {
  setZ(42);
}

void CompetitionButton::CompetitionDialog::TerrainSchema::setPos(
    double x, double y, double h) {
  mX=x; mY=y; mH=h;
  mSeq.push_back(std::pair<double,double>(x,y));
}

bool CompetitionButton::CompetitionDialog::TerrainSchema::draw(
    SDL_Surface* s) {
  FakeButton::draw(s);

  int bx = Button::mX;
  int by = Button::mY;
  int bw = Button::mW;
  int bh = Button::mH;
  int x = mX*1000;
  int y = mY*1000;
  SDL_Rect rOuter = {bx+x/3000.*bw-5,by+y/2000.*bh-5,10,10};
  SDL_FillRect(s,&rOuter,SDL_MapRGB(s->format,0,0,0));

  bool first=true;
  SDL_Renderer* r = SDL_CreateSoftwareRenderer(s);
  SDL_SetRenderDrawColor(r, 255, 0, 0, 0xFF);
  double xp,yp;
  for (const auto e : mSeq) {
    if (!first) {
      int x1 = bx+xp*1000./3000.*bw;
      int y1 = by+yp*1000./2000.*bh;
      int x2 = bx+e.first*1000./3000.*bw;
      int y2 = by+e.second*1000./2000.*bh;
      SDL_RenderDrawLine(r,x1,y1,x2,y2);
      std::cout<<" "<<x1<<" "<<y1<<" "<<x2<<" "<<y2<<std::endl;
    }
    std::cout<<xp<<" -> "<<e.first<<"   "<<yp<<" -> "<<e.second<<std::endl;
    xp=e.first;
    yp=e.second;
    first=false;
  }
  return false;
}


