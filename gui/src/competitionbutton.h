#pragma once

#include "delimitedstream.h"
#include "confirmbutton.h"
#include "runbutton.h"
#include "fakebutton.h"
#include <fstream>

class CompetitionButton : public ConfirmButton {
  public:
    using ConfirmButton::ConfirmButton;
    virtual ~CompetitionButton();

    virtual void onClickSafe();
    virtual bool draw(SDL_Surface* s);

  private:
    class CompetitionDialog : public RunButton {
      public:
        using RunButton::RunButton;
        ~CompetitionDialog();

        void init();
        void update();
        void run(std::string command);
        void abort();
      private:
        DelimitedIStream mFifo;

        class AbortButton : public ConfirmButton {
          public:
            AbortButton();
            void setParent(CompetitionDialog* p);
            
            void onClickSafe();

          private:
            CompetitionDialog* mParent = nullptr;
        };
        class BGButton : public FakeButton {
          public:
            BGButton();
        };
        class PosButton : public FakeButton {
          public:
            PosButton();
            void setPos(double x, double y, double h);
        };
        class TerrainSchema : public FakeButton {
          public:
            TerrainSchema();
            void setPos(double x, double y, double h);
            bool draw(SDL_Surface* s);
          private:
            double mX=0, mY=0, mH=0;
            std::vector<std::pair<double,double>> mSeq;
        };

        AbortButton mAbort;
        BGButton mBG;
        PosButton mPos;
        TerrainSchema mSchema;
        int mVideoPid = -1;
    };

    CompetitionDialog* mRB = nullptr;
};
