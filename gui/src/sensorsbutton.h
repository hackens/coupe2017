#pragma once

#include "confirmbutton.h"
#include "runbutton.h"
#include "com.h"
#include "fakebutton.h"

class SensorsButton : public Button {
  public:
    using Button::Button;
    virtual ~SensorsButton();

    virtual void onClick();
    virtual bool draw(SDL_Surface* s);

  protected:
    virtual bool update();

  private:
    class WatchButton : public FakeButton {
      public:
        WatchButton(std::string text, int x, int y, int w, int h,
          std::string cmd);
        std::string mCmd = nullptr;
        Com::Serial* mDevice = nullptr;
    };

    FakeButton* mRB = nullptr;
    std::vector<WatchButton*> mButtons;
    Com::Serial* mSerial = nullptr;
    Com::Serial* mSteppers = nullptr;
    time_t mSerialBegin = 0;
    bool mLoaded=false;
};
