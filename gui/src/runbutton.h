#pragma once

#include <fstream>
#include "button.h"
#include <memory>
#include <deque>

class RunButton : public Button {
  public:
    RunButton(std::string t, int x, int y, int w, int h, SDL_Color bg = {80,80,80,255}, SDL_Color tc = {255,255,255,255});
    void setLog(std::string f);
    virtual ~RunButton();

    virtual void onClick() {}
    virtual bool draw(SDL_Surface* s);
    void run(std::string command);

    bool isDone() const;

  protected:
    virtual void update();

  private:
    bool mDone = false;
    FILE* mPipe = nullptr;
    int mBufferHeight;
    int mBufferWidth;
    int mCharHeight;
    int mCharWidth;
    std::deque<std::string> mBuffer;
    SDL_Color mTC;
    std::ofstream mFile;
};

