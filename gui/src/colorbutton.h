#pragma once

#include <fstream>
#include "button.h"
#include <memory>
#include <deque>

class ColorButton : public Button {
public:
    ColorButton(std::string t, int x, int y, int w, int h, bool* boolptr, SDL_Color cfalse = {255,255,0,255}, SDL_Color ctrue = {0,0,255,255});
    virtual ~ColorButton() {}

    virtual void onClick();
    void run(std::string command);


private:
    bool* valueptr;
    SDL_Color mBGTrue, mBGFalse;
};
