#pragma once

#include "button.h"
#include "runbutton.h"

class CmdButton : public Button {
  public:
    CmdButton(std::string text, int x, int y, int w, int h, std::string cmd);
    virtual ~CmdButton();

    virtual void onClick();
    virtual bool draw(SDL_Surface* s);
  private:
    RunButton* mRB = nullptr;
    std::string mCmd;
};

class SyncButton : public Button {
  public:
    using Button::Button;
    virtual ~SyncButton();

    virtual void onClick();
    virtual bool draw(SDL_Surface* s);
  private:
    RunButton* mRB = nullptr;
};
