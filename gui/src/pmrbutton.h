#pragma once

#include "delimitedstream.h"
#include "confirmbutton.h"
#include "runbutton.h"
#include <fstream>

class PMRButton : public ConfirmButton {
  public:
    using ConfirmButton::ConfirmButton;
    virtual ~PMRButton();

    virtual void onClickSafe();
    virtual bool draw(SDL_Surface* s);

  private:
    class PMRDialog : public RunButton {
      public:
        using RunButton::RunButton;
        ~PMRDialog();

        void update();
      private:
        DelimitedIStream mFifo;
    };

    PMRDialog* mRB = nullptr;
};
