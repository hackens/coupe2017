#pragma once

#define INPUT 0
#define INPUT_PULLUP 1
#define OUTPUT 2

#include <ctime>
#include <string>

void pinMode(int pin, int mode);
void digitalWrite(int pin, int value);
int digitalRead(int pin);
int analogRead(int pin);
void analogWrite(int pin, int value);

unsigned long millis();

void delay(int millis);

typedef int boolean;
typedef std::string string;
typedef char byte;

