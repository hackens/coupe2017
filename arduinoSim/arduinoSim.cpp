#include "arduinoSim.h"
#include <assert.h>

clock_t start = clock();

int pinModes[]   = {0,0,0,0,0,0,0,0,0,0
                   ,0,0,0,0,0,0,0,0,0,0
                   ,0,0,0,0,0,0,0,0,0,0};

int pinValues[]  = {0,0,0,0,0,0,0,0,0,0
                   ,0,0,0,0,0,0,0,0,0,0
                   ,0,0,0,0,0,0,0,0,0,0};

void pinMode(int pin, int mode) {
  assert(pin>=0 && pin<30);
  pinModes[pin]=mode;
}

void digitalWrite(int pin, int value) {
  assert(pin>=0 && pin<30);
  assert(pinModes[pin]==2);
  pinValues[pin]=value;
}

int digitalRead(int pin) {
  assert(pin>=0 && pin<30);
  assert(pinModes[pin]<=1);
  return pinValues[pin]!=0;
}

int analogRead(int pin) {
  assert(pin>=0 && pin<30);
  assert(pinModes[pin]<=1);
  return pinValues[pin];
}

void analogWrite(int pin, int value) {
  assert(pin>=0 && pin<30);
  assert(pinModes[pin]==2);
  pinValues[pin]=value;
}

unsigned long millis() {
  return clock()-start;
}

void delay(int m) {
  long start=millis();
  while (millis()<start+m);
}

